from GameApp.GameApp import GameApp
from GeneratorCode.cce import Generator

import pickle
import os
import re
import numpy as np
from matplotlib import pyplot as plt


class GameCheese:
    """
    Object used to run all aspects of our CC project.
    """

    def __init__(self):
        """
        Not sure what goes here yet...
        """
        pass

    def generate_games(self, n_games, filename):
        """
        Call this to generate a number of new games using the generator code
        :param n: (int) number of games to generate
        :return: ?
        """
        game_list = []

        generator = Generator()
        try:
            from tqdm import tqdm
            for i in tqdm(range(n_games)):
                got_game = False
                while not got_game:
                    try:
                        game_list.append(generator.getGame())
                        got_game = True
                    except IndexError:
                        continue
        except ModuleNotFoundError:
            print("tqdm module not installed...")
            for i in range(n_games):
                game_list.append(generator.getGame())

        with open(filename + '.pickle', 'wb') as file:
            pickle.dump(game_list, file)

        file.close()

    def get_generated_games(self, filename):
        if filename[-7:] != '.pickle':
            filename = filename + '.pickle'
        with open(filename, 'rb') as file:
            games = pickle.load(file)
        file.close()
        return games

    def load_game(self, filename, index):
        """
        Get a game from the Knowledge Base, the generator, or elsewhere
        :param filename: string of the filename without the .pickle part
        :param index: (int) index of game you want
        :return: dictionary of the game details
        """
        with open(filename + '.pickle', 'rb') as file:
            games = pickle.load(file)
        file.close()
        return games[index]

    def save_game_data(self, games, filename):
        with open(filename + '.pickle', 'wb') as file:
            pickle.dump(games, file)
        file.close()

    def run_game_from_file(self, game_title):
        """
        Run the current game model
        :param game_title: (optional) Title of a game to run, default is currently loaded game. Throws an error if no
                            game is loaded.
        :return: None
        """
        app = GameApp()
        app.run_game_from_file(game_title)

    def eval_game(self, game, iterations, time_limit, from_file=False):
        app = GameApp()
        app.eval_game(game, iterations, time_limit, from_file=from_file)

    def run_from_gen(self, test=False):
        app = GameApp()
        app.run_game_from_gen(test=test)

    def run_saved_game(self, filename, index):
        game_dict = self.load_game(filename, index)
        app = GameApp()
        app.run_game(game_dict)

    def print_scores(self, filename):
        games = self.get_generated_games(filename)
        for i in range(len(games)):
            if "H_SCORE" in games[i].keys():
                print("index: " + str(i) + ", pe_score: " + str(games[i]['PE_SCORE']) + ", h_score: "
                      + str(games[i]['H_SCORE']))
            else:
                print("index: " + str(i) + ", pe_score: " + str(games[i]['PE_SCORE']) + ", h_score: "
                      + "not scored")

    def print_good_games(self, filename):
        games = self.get_generated_games(filename)
        for i in range(len(games)):
            if games[i]['PE_SCORE'] >= 3.0:
                if "H_SCORE" in games[i].keys():
                    print("index: " + str(i) + ", pe_score: " + str(games[i]['PE_SCORE']) + ", h_score: "
                          + str(games[i]['H_SCORE']))
                else:
                    print("index: " + str(i) + ", pe_score: " + str(games[i]['PE_SCORE']) + ", h_score: "
                          + "not scored")

    def print_game_data_from_file_idx(self, filename, index):
        games = self.get_generated_games(filename)
        self.print_game_data(games[index])

    def print_game_data(self, game):
        for key in game.keys():
            print(key)
            if isinstance(game[key], dict):
                for other_key in game[key].keys():
                    if isinstance(game[key][other_key], dict):
                        print(other_key)
                        for other_other_key in game[key][other_key].keys():
                            if isinstance(game[key][other_key][other_other_key], dict):
                                print(other_other_key)
                                for o3_key in game[key][other_key][other_other_key].keys():
                                    print("\t\t\t", o3_key, game[key][other_key][other_other_key][o3_key])
                            else:
                                print("\t\t", other_other_key, game[key][other_key][other_other_key])
                    else:
                        print("\t", other_key, game[key][other_key])
            else:
                print("\t", game[key])

    def set_game_score(self, filename, index_indices):
        games = self.get_generated_games(filename)
        if isinstance(index_indices, list):
            for index in index_indices:
                print("This is game index " + str(index) + " in file " + filename + ":")
                self.print_game_data(games[index])
                print("What score would you like to give game index " + str(index) + " in file \'" + filename + "\'?")
                score = input("Score: ")
                games[index]["H_SCORE"] = float(score)
                self.save_game_data(games, filename)
        else:
            index = index_indices
            print("This is game index " + str(index) + " in file " + filename + ":")
            self.print_game_data(games[index])
            print("What score would you like to give game index " + str(index) + " in file \'" + filename + "\'?")
            score = input("Score: ")
            games[index]["H_SCORE"] = float(score)
            self.save_game_data(games, filename)

    def accumulate_games(self):
        filenames = os.listdir("KnowledgeBase/H_evaluated")
        acc_games = []
        if isinstance(filenames, list):
            for filename in filenames:
                games = self.get_generated_games("KnowledgeBase/H_evaluated/" + filename)
                for game in games:
                    acc_games.append(game)
        else:
            games = self.get_generated_games(filenames)
            for game in games:
                acc_games.append(game)
        self.save_game_data(acc_games, "KnowledgeBase/evaluated_games")

    def count_trial_scores(self):
        filenames = os.listdir("KnowledgeBase/H_evaluated")
        scores = [0]*6
        num_games = 0
        for filename in filenames:
            games = self.get_generated_games("KnowledgeBase/H_evaluated/" + filename)
            for game in games:
                num_games += 1
                if "H_SCORE" in game.keys():
                    scores[int(game['H_SCORE'])] += 1
                else:
                    scores[int(game['PE_SCORE'])] += 1
        for i in range(len(scores)):
            print("Number of score", i, ":", scores[i])
        print("Total number of games: ", num_games)

    def scores_by_trial(self):
        filenames = os.listdir("KnowledgeBase/H_evaluated")
        filenames.sort(key=lambda x: int(re.search("_\d*_", x).group()[1:-1]))
        scores = np.zeros((6, len(filenames)))
        f = 0
        for filename in filenames:
            games = self.get_generated_games("KnowledgeBase/H_evaluated/" + filename)
            for game in games:
                if "H_SCORE" in game.keys():
                    scores[int(game['H_SCORE']), f] += 1
                else:
                    scores[int(game['PE_SCORE']), f] += 1
            f += 1
        fig, ax = plt.subplots(1, 3, sharey=True)
        fig.set_figwidth(10)
        ax[0].plot(np.arange(scores.shape[1], dtype=int), scores[0], label='Score = ' + str(0))
        ax[0].plot(np.arange(scores.shape[1], dtype=int), scores[1], label='Score = ' + str(1))
        ax[1].plot(np.arange(scores.shape[1], dtype=int), scores[2], label='Score = ' + str(2))
        ax[1].plot(np.arange(scores.shape[1], dtype=int), scores[3], label='Score = ' + str(3))
        ax[2].plot(np.arange(scores.shape[1], dtype=int), scores[4], label='Score = ' + str(4))
        ax[2].plot(np.arange(scores.shape[1], dtype=int), scores[5], label='Score = ' + str(5))
        plt.show()
        # for j in range(1, 6):
        #     if j != 1:
        #         plt.plot(np.arange(scores.shape[1], dtype=int), scores[j], label='Score = ' + str(j))
        # plt.xlabel("Trial (50 games/trial*)")
        # plt.xticks(np.arange(scores.shape[1], dtype=int))
        # plt.ylabel("Number of Games")
        # plt.title("Scores Over Time")
        # plt.legend()
        # plt.show()


if __name__ == '__main__':
    gc = GameCheese()
    # gc.run_from_gen(test=False)
    # gc.accumulate_games()
    # gc.generate_games(25, 'KnowledgeBase/test_4_25')
    # print(gc.get_generated_games('KnowledgeBase/test1'))
    gc.run_saved_game('KnowledgeBase/H_evaluated/trial_9_50', 29)
    # gc.print_scores('KnowledgeBase/test_1_25')
    # gc.print_good_games('KnowledgeBase/test_4_25')
    # gc.set_game_score('KnowledgeBase/P_evaluated/trial_20_50', [0,16,17,28,44,48])  # for the index, can give an index or list of indices
    # gc.print_game_data_from_file_idx('KnowledgeBase/H_evaluated/trial_3_50', 0)
    # gc.count_trial_scores()
    # gc.scores_by_trial()



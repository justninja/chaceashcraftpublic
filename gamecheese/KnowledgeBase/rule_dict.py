from panda3d.core import LVector3
from math import pi, sin, cos, sqrt

from GameApp.DynGameObjectInst import DynGameObjectInst

# Does nothing
def i0(object, env):
    pass

# Object dies
def i1(object, env):
    object.attributes['alive'] = False
    if not object.identifier == 'player1':
        env.stack_interaction(kill_obj,[object])

# Score increases by one
def i2(object, env):
    env.attributes["SCORE"] += 1

# Score decreases by one
def i3(object, env):
    env.attributes["SCORE"] -=1

# Object stops moving
def i4(object, env):
    setVelocity(object.panda3d_object, LVector3.zero())

# Object reverses direction
def i5(object, env):
    heading = object.panda3d_object.getR()
    heading += 180
    object.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    oldVel = getVelocity(object.panda3d_object)
    newVel = \
        LVector3(oldVel.length() * sin(heading_rad), 0, oldVel.length() * cos(heading_rad))
    setVelocity(object.panda3d_object, newVel)

# Removes z-component of movement
def i6(object, env):
    velocity = getVelocity(object.panda3d_object)
    setVelocity(object.panda3d_object, velocity.project(LVector3.unit_x()))

# Increases HP by one
def i7(object, env):
    if object.attributes['HP'] < object.attributes['MAX_HP']:
        object.attributes['HP'] += 1

# Decrease HP by one
def i8(object, env):
    if object.attributes['HP'] > 0:
        object.attributes['HP'] -= 1

interactions = [i0,i1,i2,i3,i4,i5,i6,i7,i8]

# Arrow Keys
def c0(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["up"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["down"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["right"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)

# Arrow Keys, x determines turn rate
def c1(env, player, panda_task, dt):
    keys = env.keys
    heading = player.panda3d_object.getR()  # Heading is the roll value for this model
    if keys["up"]:
        heading_rad = (pi/180) * heading
        newVel = \
            LVector3(sin(heading_rad), 0, cos(heading_rad)) * 10 * dt
        newVel += getVelocity(player.panda3d_object)
        if newVel.lengthSquared() > player.attributes["MAX_VEL"]**2:
            newVel.normalize()
            newVel *= player.attributes["MAX_VEL"]
        setVelocity(player.panda3d_object, newVel)
    if keys["down"]:
        heading_rad = (pi/180) * heading
        newVel = \
            LVector3(sin(heading_rad), 0, cos(heading_rad)) * 10 * -dt
        newVel += getVelocity(player.panda3d_object)
        if newVel.lengthSquared() > player.attributes["MAX_VEL"]**2:
            newVel.normalize()
            newVel *= player.attributes["MAX_VEL"]
        setVelocity(player.panda3d_object, newVel)
    if keys["left"]:
        heading -= dt * 360
        player.panda3d_object.setR(heading % 360)
    if keys["right"]:
        heading += dt * 360
        player.panda3d_object.setR(heading % 360)

# Arrow Keys
def c2(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["up"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["down"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["right"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)
    else:
        setVelocity(player.panda3d_object, LVector3.zero())

# Arrow Keys
def c3(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["up"] and keys["down"] and keys["left"] and keys["right"]:
        pass
    elif keys["up"] and keys["down"] and keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["up"] and keys["down"] and keys["right"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)
    elif keys["left"] and keys["right"] and keys["up"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["left"] and keys["right"] and keys["down"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["up"] and keys["down"]:
        pass
    elif keys["up"] and keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-sqrt(2) * vel / 2, 0, sqrt(2) * vel / 2))
        player.panda3d_object.setR(315)
    elif keys["up"] and keys["right"]:
        setVelocity(player.panda3d_object, LVector3(sqrt(2) * vel / 2, 0, sqrt(2) * vel / 2))
        player.panda3d_object.setR(45)
    elif keys["down"] and keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-sqrt(2) * vel / 2, 0, -sqrt(2) * vel / 2))
        player.panda3d_object.setR(225)
    elif keys["down"] and keys["right"]:
        setVelocity(player.panda3d_object, LVector3(sqrt(2) * vel / 2, 0, -sqrt(2) * vel / 2))
        player.panda3d_object.setR(135)
    elif keys["left"] and keys["right"]:
        pass
    elif keys["up"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["down"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["right"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)

# Arrow Keys
def c4(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["up"] and keys["down"] and keys["left"] and keys["right"]:
        setVelocity(player.panda3d_object, LVector3.zero())
    elif keys["up"] and keys["down"] and keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["up"] and keys["down"] and keys["right"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)
    elif keys["left"] and keys["right"] and keys["up"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["left"] and keys["right"] and keys["down"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["up"] and keys["down"]:
        setVelocity(player.panda3d_object, LVector3.zero())
    elif keys["up"] and keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-sqrt(2) * vel / 2, 0, sqrt(2) * vel / 2))
        player.panda3d_object.setR(315)
    elif keys["up"] and keys["right"]:
        setVelocity(player.panda3d_object, LVector3(sqrt(2) * vel / 2, 0, sqrt(2) * vel / 2))
        player.panda3d_object.setR(45)
    elif keys["down"] and keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-sqrt(2) * vel / 2, 0, -sqrt(2) * vel / 2))
        player.panda3d_object.setR(225)
    elif keys["down"] and keys["right"]:
        setVelocity(player.panda3d_object, LVector3(sqrt(2) * vel / 2, 0, -sqrt(2) * vel / 2))
        player.panda3d_object.setR(135)
    elif keys["left"] and keys["right"]:
        setVelocity(player.panda3d_object, LVector3.zero())
    elif keys["up"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["down"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["right"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)
    else:
        setVelocity(player.panda3d_object, LVector3.zero())

# Left/Right Arrow Keys
def c5(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["left"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0) + getVelocity(player.panda3d_object).project(LVector3.unit_z()))
        player.panda3d_object.setR(270)
    elif keys["right"]:
        setVelocity(player.panda3d_object,
                    LVector3(vel, 0, 0) + getVelocity(player.panda3d_object).project(LVector3.unit_z()))
        player.panda3d_object.setR(90)

# Left/Right Arrow Keys
def c6(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["left"]:
        setVelocity(player.panda3d_object,
                    LVector3(-vel, 0, 0) + getVelocity(player.panda3d_object).project(LVector3.unit_z()))
        player.panda3d_object.setR(270)
    elif keys["right"]:
        setVelocity(player.panda3d_object,
                    LVector3(vel, 0, 0) + getVelocity(player.panda3d_object).project(LVector3.unit_z()))
        player.panda3d_object.setR(90)
    else:
        setVelocity(player.panda3d_object, getVelocity(player.panda3d_object).project(LVector3.unit_z()))

# wasd
def c7(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["w"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["s"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["a"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["d"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)

# wasd, x determines turn rate
def c8(env, player, panda_task, dt):
    keys = env.keys
    heading = player.panda3d_object.getR()  # Heading is the roll value for this model
    if keys["w"]:
        heading_rad = (pi/180) * heading
        newVel = \
            LVector3(sin(heading_rad), 0, cos(heading_rad)) * 10 * dt
        newVel += getVelocity(player.panda3d_object)
        if newVel.lengthSquared() > player.attributes["MAX_VEL"]**2:
            newVel.normalize()
            newVel *= player.attributes["MAX_VEL"]
        setVelocity(player.panda3d_object, newVel)
    if keys["s"]:
        heading_rad = (pi/180) * heading
        newVel = \
            LVector3(sin(heading_rad), 0, cos(heading_rad)) * 10 * -dt
        newVel += getVelocity(player.panda3d_object)
        if newVel.lengthSquared() > player.attributes["MAX_VEL"]**2:
            newVel.normalize()
            newVel *= player.attributes["MAX_VEL"]
        setVelocity(player.panda3d_object, newVel)
    if keys["a"]:
        heading -= dt * 360
        player.panda3d_object.setR(heading % 360)
    if keys["d"]:
        heading += dt * 360
        player.panda3d_object.setR(heading % 360)

# wasd
def c9(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["w"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["s"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["a"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["d"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)
    else:
        setVelocity(player.panda3d_object, LVector3.zero())

# wasd
def c10(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["w"] and keys["s"] and keys["a"] and keys["d"]:
        pass
    elif keys["w"] and keys["s"] and keys["a"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["w"] and keys["s"] and keys["d"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)
    elif keys["a"] and keys["d"] and keys["w"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["a"] and keys["d"] and keys["s"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["w"] and keys["s"]:
        pass
    elif keys["w"] and keys["a"]:
        setVelocity(player.panda3d_object, LVector3(-sqrt(2) * vel / 2, 0, sqrt(2) * vel / 2))
        player.panda3d_object.setR(315)
    elif keys["w"] and keys["d"]:
        setVelocity(player.panda3d_object, LVector3(sqrt(2) * vel / 2, 0, sqrt(2) * vel / 2))
        player.panda3d_object.setR(45)
    elif keys["s"] and keys["a"]:
        setVelocity(player.panda3d_object, LVector3(-sqrt(2) * vel / 2, 0, -sqrt(2) * vel / 2))
        player.panda3d_object.setR(225)
    elif keys["s"] and keys["d"]:
        setVelocity(player.panda3d_object, LVector3(sqrt(2) * vel / 2, 0, -sqrt(2) * vel / 2))
        player.panda3d_object.setR(135)
    elif keys["a"] and keys["d"]:
        pass
    elif keys["w"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["s"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["a"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["d"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)

# wasd
def c11(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["w"] and keys["s"] and keys["a"] and keys["d"]:
        setVelocity(player.panda3d_object, LVector3.zero())
    elif keys["w"] and keys["s"] and keys["a"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["w"] and keys["s"] and keys["d"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)
    elif keys["a"] and keys["d"] and keys["w"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["a"] and keys["d"] and keys["s"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["w"] and keys["s"]:
        setVelocity(player.panda3d_object, LVector3.zero())
    elif keys["w"] and keys["a"]:
        setVelocity(player.panda3d_object, LVector3(-sqrt(2) * vel / 2, 0, sqrt(2) * vel / 2))
        player.panda3d_object.setR(315)
    elif keys["w"] and keys["d"]:
        setVelocity(player.panda3d_object, LVector3(sqrt(2) * vel / 2, 0, sqrt(2) * vel / 2))
        player.panda3d_object.setR(45)
    elif keys["s"] and keys["a"]:
        setVelocity(player.panda3d_object, LVector3(-sqrt(2) * vel / 2, 0, -sqrt(2) * vel / 2))
        player.panda3d_object.setR(225)
    elif keys["s"] and keys["d"]:
        setVelocity(player.panda3d_object, LVector3(sqrt(2) * vel / 2, 0, -sqrt(2) * vel / 2))
        player.panda3d_object.setR(135)
    elif keys["a"] and keys["d"]:
        setVelocity(player.panda3d_object, LVector3.zero())
    elif keys["w"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, vel))
        player.panda3d_object.setR(0)
    elif keys["s"]:
        setVelocity(player.panda3d_object, LVector3(0, 0, -vel))
        player.panda3d_object.setR(180)
    elif keys["a"]:
        setVelocity(player.panda3d_object, LVector3(-vel, 0, 0))
        player.panda3d_object.setR(270)
    elif keys["d"]:
        setVelocity(player.panda3d_object, LVector3(vel, 0, 0))
        player.panda3d_object.setR(90)
    else:
        setVelocity(player.panda3d_object, LVector3.zero())

# ad
def c12(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["a"]:
        setVelocity(player.panda3d_object,
                    LVector3(-vel, 0, 0) + getVelocity(player.panda3d_object).project(LVector3.unit_z()))
        player.panda3d_object.setR(270)
    elif keys["d"]:
        setVelocity(player.panda3d_object,
                    LVector3(vel, 0, 0) + getVelocity(player.panda3d_object).project(LVector3.unit_z()))
        player.panda3d_object.setR(90)

# ad
def c13(env, player, panda_task, dt):
    keys = env.keys
    vel = player.attributes['MAX_VEL']
    if keys["a"]:
        setVelocity(player.panda3d_object,
                    LVector3(-vel, 0, 0) + getVelocity(player.panda3d_object).project(LVector3.unit_z()))
        player.panda3d_object.setR(270)
    elif keys["d"]:
        setVelocity(player.panda3d_object,
                    LVector3(vel, 0, 0) + getVelocity(player.panda3d_object).project(LVector3.unit_z()))
        player.panda3d_object.setR(90)
    else:
        setVelocity(player.panda3d_object, getVelocity(player.panda3d_object).project(LVector3.unit_z()))

# Space bar (spawn obj1)
def c14(env, player, panda_task, dt):
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj1']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 2)
    env.keys["space"] = 0

# Space bar (spawn obj2)
def c15(env, player, panda_task, dt):
    if 'obj2' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj2']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 2)
    env.keys["space"] = 0

# Space bar (spawn obj3)
def c16(env, player, panda_task, dt):
    if 'obj3' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj3']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 2)
    env.keys["space"] = 0

# Space bar (spawn obj4)
def c17(env, player, panda_task, dt):
    if 'obj4' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj4']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 2)
    env.keys["space"] = 0

# Space bar (spawn obj5)
def c18(env, player, panda_task, dt):
    if 'obj5' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj5']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 2)
    env.keys["space"] = 0

# Space bar (spawn obj6)
def c19(env, player, panda_task, dt):
    if 'obj6' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj6']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 2)
    env.keys["space"] = 0

# Space bar (spawn obj7)
def c20(env, player, panda_task, dt):
    if 'obj7' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj7']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 2)
    env.keys["space"] = 0

# Space bar (spawn obj8)
def c21(env, player, panda_task, dt):
    if 'obj8' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj8']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 2)
    env.keys["space"] = 0

# Space bar (spawn obj1)
def c22(env, player, panda_task, dt):
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj1']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 5)
    env.keys["space"] = 0

# Space bar (spawn obj2)
def c23(env, player, panda_task, dt):
    if 'obj2' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj2']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 5)
    env.keys["space"] = 0

# Space bar (spawn obj3)
def c24(env, player, panda_task, dt):
    if 'obj3' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj3']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 5)
    env.keys["space"] = 0

# Space bar (spawn obj4)
def c25(env, player, panda_task, dt):
    if 'obj4' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj4']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 5)
    env.keys["space"] = 0

# Space bar (spawn obj5)
def c26(env, player, panda_task, dt):
    if 'obj5' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj5']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 5)
    env.keys["space"] = 0

# Space bar (spawn obj6)
def c27(env, player, panda_task, dt):
    if 'obj6' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj6']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 5)
    env.keys["space"] = 0

# Space bar (spawn obj7)
def c28(env, player, panda_task, dt):
    if 'obj7' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj7']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 5)
    env.keys["space"] = 0

# Space bar (spawn obj8)
def c29(env, player, panda_task, dt):
    if 'obj8' not in env.GameObjects.keys():
        return
    keys = env.keys
    if keys["space"]:
        gameObToSpawn = env.GameObjects['obj8']
        direction = (pi/180) * player.panda3d_object.getR()
        pos = player.panda3d_object.getPos()
        spawnedOb = DynGameObjectInst(gameObToSpawn, env)
        spawnedOb.panda3d_object.setPos(pos)
        vel = (getVelocity(player.panda3d_object) +
               (LVector3(sin(direction), 0, cos(direction)) *
                gameObToSpawn.attributes['INIT_VEL']))
        setVelocity(spawnedOb.panda3d_object, vel)
        # Set the object expiration time to be a certain amount past the
        # current time
        # setExpires(spawnedOb.panda3d_object, panda_task.time + gameObToSpawn.attributes['LIFETIME'])

        # Finally, add the new object to the list
        env.stack_interaction(gameObToSpawn.container.add, [spawnedOb])
        setExpires(spawnedOb.panda3d_object, panda_task.time + 5)
    env.keys["space"] = 0

# Space bar (jump)
def c30(env, player, panda_task, dt):
    keys = env.keys
    velocity = getVelocity(player.panda3d_object)
    z_velocity = velocity.project(LVector3.unit_z())
    if keys["space"] and z_velocity.length() == 0:
        setVelocity(player.panda3d_object, velocity + LVector3(0,0,player.attributes['MAX_VEL']))

controls = [c0, c1, c2, c3, c4, c5, c6, c7, c8, c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26,c27,
            c28,c29,c30]

# Win if score >= 1
def w0(env, player):
    if env.attributes['SCORE'] >= 1:
        return 1
    return 0

def ws0():
    return 'Win if score >= 1'

# Win if score >= 2
def w1(env, player):
    if env.attributes['SCORE'] >= 2:
        return 1
    return 0

# Win if score >= 5
def w2(env, player):
    if env.attributes['SCORE'] >= 5:
        return 1
    return 0

# Win if score >= 10
def w3(env, player):
    if env.attributes['SCORE'] >= 10:
        return 1
    return 0

# Win if score >= 15
def w4(env, player):
    if env.attributes['SCORE'] >= 15:
        return 1
    return 0

# Win if score >= 20
def w5(env, player):
    if env.attributes['SCORE'] >= 20:
        return 1
    return 0

# Win if score >= 25
def w6(env, player):
    if env.attributes['SCORE'] >= 25:
        return 1
    return 0

# Win if player has HP 1
def w7(env, player):
    if player.attributes['HP'] == 1:
        return 1
    return 0

# Win if player has HP 2
def w8(env, player):
    if player.attributes['HP'] == 2:
        return 1
    return 0

# Win if player has HP 5
def w9(env, player):
    if player.attributes['HP'] == 5:
        return 1
    return 0

# Win if player has HP 10
def w10(env, player):
    if player.attributes['HP'] == 10:
        return 1
    return 0

# Win if player has HP 15
def w11(env, player):
    if player.attributes['HP'] == 15:
        return 1
    return 0

# Win if player has HP 20
def w12(env, player):
    if player.attributes['HP'] == 20:
        return 1
    return 0

# Win if player has HP 25
def w13(env, player):
    if player.attributes['HP'] == 25:
        return 1
    return 0

# Win if obj1 is gone
def w14(env, player):
    if len(env.GameObjects['obj1'].container) == 0:
        return 1
    return 0

# Win if obj2 is gone
def w15(env, player):
    if 'obj2' not in env.GameObjects.keys():
        return w14(env, player)
    if len(env.GameObjects['obj2'].container) == 0:
        return 1
    return 0

# Win if obj3 is gone
def w16(env, player):
    if 'obj3' not in env.GameObjects.keys():
        return w15(env, player)
    if len(env.GameObjects['obj3'].container) == 0:
        return 1
    return 0

# Win if obj4 is gone
def w17(env, player):
    if 'obj4' not in env.GameObjects.keys():
        return w16(env, player)
    if len(env.GameObjects['obj4'].container) == 0:
        return 1
    return 0

# Win if obj5 is gone
def w18(env, player):
    if 'obj5' not in env.GameObjects.keys():
        return w17(env, player)
    if len(env.GameObjects['obj5'].container) == 0:
        return 1
    return 0

# Win if obj6 is gone
def w19(env, player):
    if 'obj6' not in env.GameObjects.keys():
        return w18(env, player)
    if len(env.GameObjects['obj6'].container) == 0:
        return 1
    return 0

# Win if obj7 is gone
def w20(env, player):
    if 'obj7' not in env.GameObjects.keys():
        return w19(env, player)
    if len(env.GameObjects['obj7'].container) == 0:
        return 1
    return 0

# Win if obj8 is gone
def w21(env, player):
    if 'obj8' not in env.GameObjects.keys():
        return w20(env, player)
    if len(env.GameObjects['obj8'].container) == 0:
        return 1
    return 0

# Lose if player is dead
def f0(env, player):
    if player.attributes['alive'] is False:
        return -1
    return 0

# Lose if player has HP 0
def f1(env, player):
    if player.attributes['HP'] == 0:
        return -1
    return 0

# Lose if obj1 is gone
def f2(env, player):
    if len(env.GameObjects['obj1'].container) == 0:
        return -1
    return 0

# Lose if obj2 is gone
def f3(env, player):
    if 'obj2' not in env.GameObjects.keys():
        return f2(env, player)
    if len(env.GameObjects['obj2'].container) == 0:
        return -1
    return 0

# Lose if obj3 is gone
def f4(env, player):
    if 'obj3' not in env.GameObjects.keys():
        return f3(env, player)
    if len(env.GameObjects['obj3'].container) == 0:
        return -1
    return 0

# Lose if obj4 is gone
def f5(env, player):
    if 'obj4' not in env.GameObjects.keys():
        return f4(env, player)
    if len(env.GameObjects['obj4'].container) == 0:
        return -1
    return 0

# Lose if obj5 is gone
def f6(env, player):
    if 'obj5' not in env.GameObjects.keys():
        return f5(env, player)
    if len(env.GameObjects['obj5'].container) == 0:
        return -1
    return 0

# Lose if obj6 is gone
def f7(env, player):
    if 'obj6' not in env.GameObjects.keys():
        return f6(env, player)
    if len(env.GameObjects['obj6'].container) == 0:
        return -1
    return 0

# Lose if obj7 is gone
def f8(env, player):
    if 'obj7' not in env.GameObjects.keys():
        return f7(env, player)
    if len(env.GameObjects['obj7'].container) == 0:
        return -1
    return 0

# Lose if obj8 is gone
def f9(env, player):
    if 'obj8' not in env.GameObjects.keys():
        return f8(env, player)
    if len(env.GameObjects['obj8'].container) == 0:
        return -1
    return 0

def f10(env, player):
    if player.panda3d_object.getPos().getZ() < -env.attributes["SCREEN_Y"]:
        return -1
    return 0

objectives = [w0,w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11,w12,w13,w14,w15,w16,w17,w18,w19,w20,w21,f0,f1,f2,f3,f4,f5,f6,
              f7,f8,f9,f10]

win_objectives = [w0,w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11,w12,w13,w14,w15,w16,w17,w18,w19,w20,w21]

lose_objectives = [f0,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10]

# Does nothing (continue forward
def b0(env, obj, panda_task, dt):
    pass

# Turns in a circle clockwise
def b1(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()  # Heading is the roll value for this model
    heading += 1 * dt * 360 / 10
    obj.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 30 * 1 * dt / 10
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Turns in a circle counterclockwise
def b2(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()  # Heading is the roll value for this model
    heading -= 1 * dt * 360 / 10
    obj.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 30 * 1 * dt / 10
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Accelerates to max velocity
def b3(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 1 * dt
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Decelerates to zero velocity
def b4(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()
    heading_rad = (pi / 180) * heading
    newVel = getVelocity(obj.panda3d_object)
    if newVel.length() <= 1:
        newVel = LVector3.zero()
    else:
        newVel -= LVector3(sin(heading_rad), 0, cos(heading_rad)) * 1 * dt
    setVelocity(obj.panda3d_object, newVel)

# Turns in a circle clockwise
def b5(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()  # Heading is the roll value for this model
    heading += 2 * dt * 360 / 10
    obj.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 30 * 2 * dt / 10
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Turns in a circle counterclockwise
def b6(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()  # Heading is the roll value for this model
    heading -= 2 * dt * 360 / 10
    obj.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 30 * 2 * dt / 10
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Turns in a circle clockwise
def b7(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()  # Heading is the roll value for this model
    heading += 3 * dt * 360 / 10
    obj.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 30 * 3 * dt / 10
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Turns in a circle counterclockwise
def b8(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()  # Heading is the roll value for this model
    heading -= 3 * dt * 360 / 10
    obj.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 30 * 3 * dt / 10
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Turns in a circle clockwise
def b9(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()  # Heading is the roll value for this model
    heading += 4 * dt * 360 / 10
    obj.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 30 * 4 * dt / 10
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Turns in a circle counterclockwise
def b10(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()  # Heading is the roll value for this model
    heading -= 4 * dt * 360 / 10
    obj.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 30 * 4 * dt / 10
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Turns in a circle clockwise
def b11(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()  # Heading is the roll value for this model
    heading += 4 * dt * 360 / 10
    obj.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 30 * 4 * dt / 10
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Turns in a circle counterclockwise
def b12(env, obj, panda_task, dt):
    heading = obj.panda3d_object.getR()  # Heading is the roll value for this model
    heading -= 0.5 * dt * 360 / 10
    obj.panda3d_object.setR(heading % 360)
    heading_rad = (pi / 180) * heading
    newVel = \
        LVector3(sin(heading_rad), 0, cos(heading_rad)) * 30 * 0.5 * dt / 10
    newVel += getVelocity(obj.panda3d_object)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

# Fall (decrease z velocity by one)
def b13(env, obj, panda_task, dt):
    newVel = getVelocity(obj.panda3d_object)
    newVel = newVel + LVector3(0,0,-0.1)
    if newVel.lengthSquared() > obj.attributes["MAX_VEL"] ** 2:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)

def b14(env, obj, panda_task, dt):
    obj_pos = obj.panda3d_object.getPos()
    player_pos = env.Players['player1'].panda3d_object.getPos()
    newVel = LVector3(player_pos.getX() - obj_pos.getX(),0,player_pos.getZ() - obj_pos.getZ())
    if newVel.length is not 0:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    setVelocity(obj.panda3d_object, newVel)
    if sqrt((obj_pos.getX() - player_pos.getX())**2 + (obj_pos.getZ() - player_pos.getZ())**2) < 20:
        setVelocity(obj.panda3d_object, newVel)

def b15(env, obj, panda_task, dt):
    obj_pos = obj.panda3d_object.getPos()
    player_pos = env.Players['player1'].panda3d_object.getPos()
    newVel = LVector3(obj_pos.getX() - player_pos.getX(), 0, obj_pos.getZ() - player_pos.getZ())
    if newVel.length is not 0:
        newVel.normalize()
        newVel *= obj.attributes["MAX_VEL"]
    if sqrt((obj_pos.getX() - player_pos.getX())**2 + (obj_pos.getZ() - player_pos.getZ())**2) < 20:
        setVelocity(obj.panda3d_object, newVel)

behaviors = [b0, b1, b2, b3, b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15]

def l0(x):
    return ((x) % 40) - 20
def l1(x):
    return ((x + 5) % 40) - 20
def l2(x):
    return ((x + 10) % 40) - 20
def l3(x):
    return ((x + 15) % 40) - 20
def l4(x):
    return ((x + 20) % 40) - 20
def l5(x):
    return ((x + 25) % 40) - 20
def l6(x):
    return ((x + 30) % 40) - 20
def l7(x):
    return ((x + 35) % 40) - 20
def l8(x):
    return 20 - ((x) % 40)
def l9(x):
    return 20 - ((x + 5) % 40)
def l10(x):
    return 20 - ((x + 10) % 40)
def l11(x):
    return 20 - ((x + 15) % 40)
def l12(x):
    return 20 - ((x + 20) % 40)
def l13(x):
    return 20 - ((x + 25) % 40)
def l14(x):
    return 20 - ((x + 30) % 40)
def l15(x):
    return 20 - ((x + 35) % 40)
def l16(x):
    return ((2 * x) % 40) - 20
def l17(x):
    return ((2 * x + 10) % 40) - 20
def l18(x):
    return ((2 * x + 20) % 40) - 20
def l19(x):
    return ((2 * x + 30) % 40) - 20
def l20(x):
    return 20 - ((2 * x) % 40)
def l21(x):
    return 20 - ((2 * x + 10) % 40)
def l22(x):
    return 20 - ((2 * x + 20) % 40)
def l23(x):
    return 20 - ((2 * x + 30) % 40)
def l24(x):
    return ((5 * x) % 40) - 20
def l25(x):
    return ((5 * x + 10) % 40) - 20
def l26(x):
    return ((5 * x + 20) % 40) - 20
def l27(x):
    return ((5 * x + 30) % 40) - 20
def l28(x):
    return 20 - ((5 * x) % 40)
def l29(x):
    return 20 - ((5 * x + 10) % 40)
def l30(x):
    return 20 - ((5 * x + 20) % 40)
def l31(x):
    return 20 - ((5 * x + 30) % 40)
def l32(x):
    return ((10 * x) % 40) - 20
def l33(x):
    return ((10 * x + 10) % 40) - 20
def l34(x):
    return ((10 * x + 20) % 40) - 20
def l35(x):
    return ((10 * x + 30) % 40) - 20
def l36(x):
    return 20 - ((10 * x) % 40)
def l37(x):
    return 20 - ((10 * x + 10) % 40)
def l38(x):
    return 20 - ((10 * x + 20) % 40)
def l39(x):
    return 20 - ((10 * x + 30) % 40)
def l40(x):
    return (x % 20) - 20
def l41(x):
    return (x % 20)
def l42(x):
    return 20 - (x % 20)
def l43(x):
    return -1 * (x % 20)
def l44(x):
    return (x % 10) - 20
def l45(x):
    return (x % 10) - 10
def l46(x):
    return (x % 10)
def l47(x):
    return (x % 10) + 10
def l48(x):
    return (x % 5) - 20
def l49(x):
    return (x % 5) - 15
def l50(x):
    return (x % 5) - 10
def l51(x):
    return (x % 5) - 5
def l52(x):
    return (x % 5)
def l53(x):
    return (x % 5) + 5
def l54(x):
    return (x % 5) + 10
def l55(x):
    return (x % 5) + 15
def l56(x):
    return -20
def l57(x):
    return -19
def l58(x):
    return -18
def l59(x):
    return -17
def l60(x):
    return -16
def l61(x):
    return -15
def l62(x):
    return -14
def l63(x):
    return -13
def l64(x):
    return -12
def l65(x):
    return -11
def l66(x):
    return -10
def l67(x):
    return -9
def l68(x):
    return -8
def l69(x):
    return -7
def l70(x):
    return -6
def l71(x):
    return -5
def l72(x):
    return -4
def l73(x):
    return -3
def l74(x):
    return -2
def l75(x):
    return -1
def l76(x):
    return 0
def l77(x):
    return 1
def l78(x):
    return 2
def l79(x):
    return 3
def l80(x):
    return 4
def l81(x):
    return 5
def l82(x):
    return 6
def l83(x):
    return 7
def l84(x):
    return 8
def l85(x):
    return 9
def l86(x):
    return 10
def l87(x):
    return 11
def l88(x):
    return 12
def l89(x):
    return 13
def l90(x):
    return 14
def l91(x):
    return 15
def l92(x):
    return 16
def l93(x):
    return 17
def l94(x):
    return 18
def l95(x):
    return 19

locations = [l0,l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15,l16,l17,l18,l19,l20,l21,l22,l23,l24,l25,l26,l27,l28,l29,
             l30,l31,l32,l33,l34,l35,l36,l37,l38,l39,l40,l41,l42,l43,l44,l45,l46,l47,l48,l49,l50,l51,l52,l53,l54,l55,l56,
             l57,l58,l59,l60,l61,l62,l63,l64,l65,l66,l67,l68,l69,l70,l71,l72,l73,l74,l75,l76,l77,l78,l79,l80,l81,l82,l83,l84,
             l85,l86,l87,l88,l89,l90,l91,l92,l93,l94,l95]

# ---------------------------Attributes---------------------------------------------

req_player_attributes = ["initial_key_values"]
req_obj_attributes = ["INIT_VEL", "MAX_VEL"]
req_env_attributes = ["initial_key_values", "SCORE"]

opt_player_attributes = ['alive']
opt_obj_attributes = ["static"]
opt_env_attributes = []

attributes = {"player_required": req_player_attributes,
              "object_required": req_obj_attributes,
              "environment_required": req_env_attributes,
              "player_optional": opt_player_attributes,
              "object_optional": opt_obj_attributes,
              "environment_optional": opt_env_attributes}

#---------------------------HELPER FUNCTIONS----------------------------------------

def setVelocity(obj, val):
    obj.setPythonTag("velocity", val)

def getVelocity(obj):
    return obj.getPythonTag("velocity")

def setExpires(obj, val):
    obj.setPythonTag("expires", val)

def getExpires(obj):
    return obj.getPythonTag("expires")

def kill_obj(object):
    object.remove_self()
    # object.panda3d_object.removeNode()
    # object.parent.container.remove(object)
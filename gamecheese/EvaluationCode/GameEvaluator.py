from GameApp.GameApp import GameApp
from gamecheese import GameCheese

from multiprocessing import Process
import numpy as np
import pickle
import os


def f_eval2(game, iters, time_limit, save_info=None):
    app = GameApp()
    app.eval_game(game, iters, time_limit, save_info=save_info)


def save_scores(games_list, gamefile, scorefile, n_games):
    score_array = np.load(scorefile + ".npy")
    for j in range(n_games):
        games_list[j]['PE_SCORE'] = score_array[j]

    with open(gamefile + '.pickle', 'wb') as file:
        pickle.dump(games_list, file)

    file.close()


if __name__ == '__main__':
    print(os.getcwd())
    os.chdir("..")
    game_filename = 'KnowledgeBase/trial_20_50'
    print(game_filename)
    iterations = 5
    game_time_limit_sec = 30
    temp_file = game_filename + "_eval_temp"

    gc = GameCheese()
    games = gc.get_generated_games(game_filename)
    num_games = len(games)

    for i in range(len(games)):
        print("Game " + str(i+1) + " out of " + str(len(games)))
        save_info = (temp_file, i, num_games)
        args = (games[i], iterations, game_time_limit_sec)
        kwargs = {'save_info': save_info}
        p = Process(target=f_eval2, args=args, kwargs=kwargs)
        p.start()
        p.join()

    save_scores(games, game_filename, temp_file, num_games)
    os.remove(temp_file + '.npy')

import random
import pickle
import numpy as np


class GameAgent:
    """
    Agent that plays the generated game for phenotypic evaluation.
    """

    def __init__(self, env, iterations, game_time_limit, save_info=None, agent_type=None):
        """
        Initialize agent
        :param env: (GameEvaluator) A reference to the current GameEvaluator object.
        :param env: (Environment) A reference to the current game environment. Needed to get game
            information and keys
        :param type: (string) A string indicating the behavior of the string. Options are:
            'random',...
            Default is 'random1'.
        """
        self.spec_agent = False
        if agent_type is not None:
            self.spec_agent = True
            self.agent_type = agent_type
        else:
            self.agent_type = 0
        self.save_info = save_info
        self.agent_types = ['random1', 'random2', 'shooter', 'wander', 'wander2', 'wander3']
        # self.agent_types = ['wander2', 'wander3']
        self.env = env
        self.key_pressed = False
        self.cur_key_presses = {}
        self.game_time = 0
        self.won_or_lost = False

        self.wins = 0
        self.losses = 0

        self.iterations = iterations
        self.game_time_limit = game_time_limit
        self.iter = 0
        self.game_one = True
        self.win_cap_percent = 0.25
        self.win_cap = self.win_cap_percent*self.iterations*len(self.agent_types)

        self.wins_dict = {}
        self.losses_dict = {}

    def save_data(self):
        # only used with spec agent
        self.wins_dict[self.agent_types[self.agent_type]] = self.wins
        self.losses_dict[self.agent_types[self.agent_type]] = self.losses
        print([self.wins, self.losses])

    def score_game(self, insta_end=False):
        if insta_end:
            score = 1
        elif self.wins <= np.maximum(2, self.wins):
            score = 3
        else:
            score = 2

        save_file = self.save_info[0]
        game_index = self.save_info[1]
        game_array_size = self.save_info[2]

        try:
            game_array = np.load(save_file+'.npy')
            game_array[game_index] = score
            np.save(save_file + '.npy', game_array)
        except FileNotFoundError:
            # file did not exist, create array and save it
            game_array = np.zeros(game_array_size)
            game_array[game_index] = score
            np.save(save_file + '.npy', game_array)
        print("scored")

    def eval_play(self, panda_task, dt):
        if self.env.attributes["STATE"] == 0:
            if self.won_or_lost:
                self.won_or_lost = False
            self.game_time += dt
            if self.game_time > self.game_time_limit:
                self.env.attributes["STATE"] = -1
            else:
                if self.agent_types[self.agent_type] == 'random1':
                    self.random1_play(panda_task)
                elif self.agent_types[self.agent_type] == 'random2':
                    # Little more eccentric behavior
                    self.random2_play(panda_task)
                elif self.agent_types[self.agent_type] == 'shooter':
                    self.shooter_play(panda_task)
                elif self.agent_types[self.agent_type] == 'wander':
                    self.wander_play(panda_task)
                elif self.agent_types[self.agent_type] == 'wander2':
                    self.wander2_play(panda_task)
                elif self.agent_types[self.agent_type] == 'wander3':
                    self.wander3_play(panda_task)
        elif self.env.attributes["STATE"] == -1:
            if self.game_one:
                if self.game_time < 0.8:
                    self.score_game(insta_end=True)
                    return -1
                else:
                    self.game_one = False
            if not self.won_or_lost:
                self.losses += 1
                self.won_or_lost = True
                print('lose')
                self.env.keys['restart'] = 1
                self.iter += 1
                self.game_time = 0
                if self.iter >= self.iterations:
                    print("next_agent")
                    self.agent_type += 1
                    if self.agent_type >= len(self.agent_types):
                        # evaluate game and save score
                        self.score_game()
                        return -1
                    self.iter = 0
        elif self.env.attributes["STATE"] == 1:
            if self.game_one:
                if self.game_time < 0.8:
                    self.score_game(insta_end=True)
                    return -1
                else:
                    self.game_one = False
            if self.wins > self.win_cap:
                self.score_game(insta_end=True)
                return -1
            if not self.won_or_lost:
                print("win")
                self.wins += 1
                self.won_or_lost = True
                self.env.keys['restart'] = 1
                self.iter += 1
                self.game_time = 0
                if self.iter >= self.iterations:
                    print("next_agent")
                    self.agent_type += 1
                    if self.agent_type >= len(self.agent_types):
                        # evaluate game and save score
                        self.score_game()
                        return -1
                    self.iter = 0

    def play_game(self, panda_task, dt):
        if self.spec_agent:
            return self.play_spec_agent(panda_task, dt)
        else:
            return self.eval_play(panda_task, dt)

    def random1_play(self, panda_task):
        if self.key_pressed:
            for key in list(self.cur_key_presses.keys()):
                if self.cur_key_presses[key] < panda_task.time:
                    self.env.keys[key] = 0
                    del self.cur_key_presses[key]
            no_press = True
            for key, val in self.env.keys.items():
                if val > 0:
                    no_press = False
                    break
            if no_press:
                self.key_pressed = False
                self.cur_key_presses.clear()

        if random.random() < 0.9 and len(self.cur_key_presses) <= 2:  # 10% of the time, agent does nothing and only 3
            #  keys may be pressed at a time
            eligible_keys = set(self.env.keys.keys())
            if 'exit' in eligible_keys:
                eligible_keys.remove('exit')
            key_press = random.choice(list(eligible_keys))
            self.env.keys[key_press] = 1
            self.cur_key_presses[key_press] = panda_task.time + random.uniform(0.1, 0.7)
            self.key_pressed = True

    def random2_play(self, panda_task):
        if self.key_pressed:
            for key in list(self.cur_key_presses.keys()):
                if self.cur_key_presses[key] < panda_task.time:
                    self.env.keys[key] = 0
                    del self.cur_key_presses[key]
            no_press = True
            for key, val in self.env.keys.items():
                if val > 0:
                    no_press = False
                    break
            if no_press:
                self.key_pressed = False
                self.cur_key_presses.clear()

        if random.random() < 0.95:  # 5% of the time, agent does nothing, all keys may be pressed.
            eligible_keys = set(self.env.keys.keys())
            if 'exit' in eligible_keys:
                eligible_keys.remove('exit')
            key_press = random.choice(list(eligible_keys))
            self.env.keys[key_press] = 1
            self.cur_key_presses[key_press] = panda_task.time + random.uniform(0.1, 0.7)
            self.key_pressed = True

    def shooter_play(self, panda_task):
        if self.key_pressed:
            for key in list(self.cur_key_presses.keys()):
                if self.cur_key_presses[key] < panda_task.time:
                    self.env.keys[key] = 0
                    del self.cur_key_presses[key]
            no_press = True
            for key, val in self.env.keys.items():
                if val > 0:
                    no_press = False
                    break
            if no_press:
                self.key_pressed = False
                self.cur_key_presses.clear()

        if random.random() < 0.99:  # 1% of the time, agent does nothing, all keys may be pressed.
            eligible_keys = set(self.env.keys.keys())
            if 'exit' in eligible_keys:
                eligible_keys.remove('exit')
            key_press = random.choice(list(eligible_keys))
            self.env.keys[key_press] = 1
            self.env.keys['space'] = 1
            self.cur_key_presses[key_press] = panda_task.time + random.uniform(0.1, 0.7)
            self.cur_key_presses[key_press] = panda_task.time + random.uniform(0.01, 0.2)
            self.key_pressed = True

    def wander_play(self, panda_task):
        if self.key_pressed:
            for key in list(self.cur_key_presses.keys()):
                if self.cur_key_presses[key] < panda_task.time:
                    self.env.keys[key] = 0
                    del self.cur_key_presses[key]
            no_press = True
            for key, val in self.env.keys.items():
                if val > 0:
                    no_press = False
                    break
            if no_press:
                self.key_pressed = False
                self.cur_key_presses.clear()

        if random.random() < 0.9 and len(self.cur_key_presses) <= 1:  # 10% of the time, agent does nothing, and only 2
            #  keys may be pressed at a time
            eligible_keys = set(self.env.keys.keys())
            if 'exit' in eligible_keys:
                eligible_keys.remove('exit')
            if 'space' in eligible_keys:
                eligible_keys.remove('space')
            key_press = random.choice(list(eligible_keys))
            self.env.keys[key_press] = 1
            self.cur_key_presses[key_press] = panda_task.time + random.uniform(2, 6)
            self.key_pressed = True

    def wander2_play(self, panda_task):
        if self.key_pressed:
            for key in list(self.cur_key_presses.keys()):
                if self.cur_key_presses[key] < panda_task.time:
                    self.env.keys[key] = 0
                    del self.cur_key_presses[key]
            no_press = True
            for key, val in self.env.keys.items():
                if val > 0:
                    no_press = False
                    break
            if no_press:
                self.key_pressed = False
                self.cur_key_presses.clear()

        if random.random() < 0.5 and len(self.cur_key_presses) <= 1:  # 50% of the time, agent does nothing, and only 2
            #  keys may be pressed at a time
            eligible_keys = set(self.env.keys.keys())
            if 'exit' in eligible_keys:
                eligible_keys.remove('exit')
            if 'space' in eligible_keys:
                eligible_keys.remove('space')
            key_press = random.choice(list(eligible_keys))
            self.env.keys[key_press] = 1
            self.cur_key_presses[key_press] = panda_task.time + random.uniform(5, 7)
            self.key_pressed = True

    def wander3_play(self, panda_task):
        if self.key_pressed:
            for key in list(self.cur_key_presses.keys()):
                if self.cur_key_presses[key] < panda_task.time:
                    self.env.keys[key] = 0
                    del self.cur_key_presses[key]
            no_press = True
            for key, val in self.env.keys.items():
                if val > 0:
                    no_press = False
                    break
            if no_press:
                self.key_pressed = False
                self.cur_key_presses.clear()

        if random.random() < 0.25 and len(self.cur_key_presses) < 1:  # 25% of the time, agent does nothing, and only 1
            #  key may be pressed at a time
            eligible_keys = set(self.env.keys.keys())
            if 'exit' in eligible_keys:
                eligible_keys.remove('exit')
            if 'space' in eligible_keys:
                eligible_keys.remove('space')
            key_press = random.choice(list(eligible_keys))
            self.env.keys[key_press] = 1
            self.cur_key_presses[key_press] = panda_task.time + random.uniform(5, 10)
            self.key_pressed = True

    def play_spec_agent(self, panda_task, dt):
        if self.env.attributes["STATE"] == 0:
            if self.won_or_lost:
                self.won_or_lost = False
            self.game_time += dt
            if self.game_time > self.game_time_limit:
                self.env.attributes["STATE"] = -1
            else:
                if self.agent_types[self.agent_type] == 'random1':
                    self.random1_play(panda_task)
                elif self.agent_types[self.agent_type] == 'random2':
                    # Little more eccentric behavior
                    self.random2_play(panda_task)
        elif self.env.attributes["STATE"] == -1:
            if self.iter == 0 and self.game_time < 0.5:
                self.score_game(insta_end=True)
                return -1
            if not self.won_or_lost:
                self.losses += 1
                self.won_or_lost = True
                print('lose')
                self.env.keys['restart'] = 1
                self.iter += 1
                self.game_time = 0
                if self.iter >= self.iterations:
                    return -1
        elif self.env.attributes["STATE"] == 1:
            if self.iter == 0 and self.game_time < 0.5:
                return -1
            if not self.won_or_lost:
                print("win")
                self.wins += 1
                self.won_or_lost = True
                self.env.keys['restart'] = 1
                self.iter += 1
                self.game_time = 0
                if self.iter >= self.iterations:
                    print("next_agent")
                    self.save_data()
                    return -1

This directory should contain the code used to evaluate genotypic artifacts and phenotypic artifacts produced by the
game generator. For example, we considered implementing an AI to try to play some of the generated games via the game
app. That code would go in this director, perhaps in a sub-director labeled "AI."
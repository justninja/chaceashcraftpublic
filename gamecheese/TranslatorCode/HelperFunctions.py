# Helper functions for GOT and POT classes.


def updatePos(obj, env, dt):
    vel = getVelocity(obj)
    newPos = obj.getPos() + (vel * dt)

    # Check if the object is out of bounds. If so, wrap it
    radius = .5 * obj.getScale().getX()
    if newPos.getX() - radius > env.attributes["SCREEN_X"]:
        newPos.setX(-env.attributes["SCREEN_X"])
    elif newPos.getX() + radius < -env.attributes["SCREEN_X"]:
        newPos.setX(env.attributes["SCREEN_X"])
    if newPos.getZ() - radius > env.attributes["SCREEN_Y"]:
        newPos.setZ(-env.attributes["SCREEN_Y"])
    elif newPos.getZ() + radius < -env.attributes["SCREEN_Y"]:
        newPos.setZ(env.attributes["SCREEN_Y"])

    obj.setPos(newPos)


def getVelocity(obj):
    return obj.getPythonTag("velocity")


def setVelocity(obj, val):
    obj.setPythonTag("velocity", val)
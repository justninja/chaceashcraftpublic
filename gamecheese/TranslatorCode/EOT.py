from TranslatorCode.GOT import GOT
from TranslatorCode.POT import POT

from KnowledgeBase.rule_dict import interactions
from KnowledgeBase.rule_dict import controls
from KnowledgeBase.rule_dict import win_objectives
from KnowledgeBase.rule_dict import lose_objectives

from panda3d.core import LVector3
from direct.gui.OnscreenText import OnscreenText
from direct.task.Task import Task

import sys
import numpy as np


class EOT:
    """
    Stands for Environment Object Translator. Takes generated vectors and turns them into functions that can be passed
    into the GameApp class.
    """
    def __init__(self, generator_dict):
        """
        Initialize the object.
        :param generator_dict:
        """
        self.generated = generator_dict
        # self.update_list = np.where(np.array(generator_dict['update']) > 0)
        self.attributes = {"SCREEN_X": 20,
                          "SCREEN_Y": 15,
                          "initial_key_values": {"exit": 0, "restart": 0},
                          "SCORE": 0,
                          "STATE": 0,
                          "score_text_object": None,
                          "win_lose_state": None
                          }
        # self.interaction_list = np.where(np.array(generator_dict['interaction']) > 0)
        # self.FSM = generator_dict['FSM']

        # self.num_objectives = len(objectives)
        self.win_objectives = list(np.where(np.array(generator_dict['win']) > 0)[0])
        self.lose_objectives = list(np.where(np.array(generator_dict['lose']) > 0)[0])

        self.environment_actions = {"escape": ["exit", 1], "r": ["restart", 1], "r-up": ["restart", 0]}

    def check_objectives(self, env, player):
        for i in self.win_objectives:
            result = win_objectives[i](env, player)
            if not result == 0:
                return result
        for i in self.lose_objectives:
            result = lose_objectives[i](env, player)
            if not result == 0:
                return result
        return 0

    def environment_initializer(self, env):
        env.attributes['score_text_object'] = OnscreenText(text='Score: 0', pos=(-1.18, 0.93), scale=0.07,
                                                           mayChange=True,
                                                           fg=(1, 1, 0, 1))
        env.attributes['win_lose_state'] = OnscreenText(text='', pos=(0, 0, 0), scale=0.45, mayChange=True,
                                                        fg=(1, 1, 0, 1))

    def environment_behavior(self, env, panda_task, dt):
        # TODO Fix this?
        if env.keys['exit']:
            sys.exit()

        if env.attributes['STATE'] == 0:
            env.attributes['STATE'] = self.check_objectives(env, env.Players['player1'])
            env.attributes['score_text_object'].setText("Score: " + str(env.attributes["SCORE"]))

        if env.attributes['STATE'] != 0 and env.keys['restart']:
            env.attributes['win_lose_state'].setText("")
            env.attributes['score_text_object'].destroy()
            env.restart = True
        elif env.attributes['STATE'] is 1:
            env.attributes['win_lose_state'].setText("YOU WIN")
        elif env.attributes['STATE'] is -1:
            env.attributes['win_lose_state'].setText("GAME OVER\n MAN")

        return Task.cont  # Since every return is Task.cont, the task will
        # continue indefinitely

    def environment_interaction(self, player, env, other_obj):
        pass

    def dictionary(self):
        used_locations = set()
        players = {'player1': POT('player1', self.generated['player1'], used_locations).dictionary()}
        # make sure player is set first
        game_objects = {}
        for identifier in self.generated['game_objects'].keys():
            game_objects[identifier] = GOT(identifier, self.generated['game_objects'][identifier], used_locations)\
                .dictionary()

        return {"initializer": self.environment_initializer,
                       "attributes": self.attributes,
                       "players": players,
                       "game_objects": game_objects,
                       "actions": self.environment_actions,
                       "interaction": self.environment_interaction,
                       "update": self.environment_behavior,
                       "FSM": None}

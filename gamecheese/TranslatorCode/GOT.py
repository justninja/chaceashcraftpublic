from TranslatorCode.HelperFunctions import *
from GameApp.DynGameObjectInst import DynGameObjectInst

from KnowledgeBase.rule_dict import interactions
from KnowledgeBase.rule_dict import behaviors
from KnowledgeBase.rule_dict import locations
from KnowledgeBase.rule_dict import getExpires

from random import random
import numpy as np
from math import pi, sin, cos

from panda3d.core import LVector3


class GOT:
    """
    Stands for Game Object Translator. Takes generated vectors and turns them into functions that can be passed into
    the GameApp class.
    """
    def __init__(self, identifier, generator_dict, used_positions_ptr):
        """
        Initialize the object.
        :param identifier:
        :param generator_dict:
        """
        self.generated = generator_dict
        self.identifier = identifier
        self.static = generator_dict['static']
        self.p3do_dict = {"tex": identifier + ".png", 'scale': 1.0}
        self.update_list = list(np.where(np.array(generator_dict['update']) > 0)[0])
        self.attributes = generator_dict['attributes']
        self.interaction_dict = generator_dict['interaction']
        # self.interaction_list = np.where(np.array(generator_dict['interaction']) > 0)[0]
        self.FSM = generator_dict['FSM']

        self.used_positions = used_positions_ptr
        self.x_func_idx = generator_dict['x_func']
        self.y_func_idx = generator_dict['y_func']

        self.used_positions_so_far = list(used_positions_ptr)
        self.new_positions = []

    def is_position_available(self, x, y):
        for point in self.used_positions_so_far:
            if np.sqrt((point[0] - x)**2 + (point[1] - y)**2) < 2:
                return False
        return True

    def get_object_positions(self):
        positions = []
        iters = 0
        while len(positions) < self.generated['init_num_objects']:
            worked = False
            while iters < 100 and not worked:
                x_pos = locations[self.x_func_idx](iters)
                y_pos = locations[self.y_func_idx](iters)
                if self.is_position_available(x_pos, y_pos):
                    worked = True
                    positions.append((x_pos, y_pos))
                iters += 1
        for position in positions:
            self.used_positions.add(position)
        return positions

    def obj_initializer(self, env, objGameObj):
        if not self.static:
            objGameObj.container = set()
            positions = self.get_object_positions()
            for i in range(len(positions)):
                objGameObj.panda3d_object['tex'] = self.identifier + ".png"
                obj = DynGameObjectInst(objGameObj, env)
                objGameObj.container.add(obj)
                obj.panda3d_object.setX(positions[i][0])
                obj.panda3d_object.setZ(positions[i][1])

                # Heading is a random angle in radians
                heading = random() * 2 * pi

                # Converts the heading to a vector and multiplies it by speed to
                # get a velocity vector
                v = LVector3(sin(heading), 0, cos(heading)) * objGameObj.attributes['INIT_VEL']
                setVelocity(obj.panda3d_object, v)
        else:
            x_pos = locations[self.x_func_idx](0)
            y_pos = locations[self.y_func_idx](0)
            iters = 1
            while not self.is_position_available(x_pos, y_pos) and iters < 100:
                x_pos = locations[self.x_func_idx](iters)
                y_pos = locations[self.y_func_idx](iters)
                iters += 1
            objGameObj.panda3d_object.setX(x_pos)
            objGameObj.panda3d_object.setZ(y_pos)
            self.used_positions.add((x_pos, y_pos))

            heading = random() * 2 * pi
            v = LVector3(sin(heading), 0, cos(heading)) * objGameObj.attributes['INIT_VEL']
            setVelocity(objGameObj.panda3d_object, v)

    def obj_behavior(self, obj, env, panda_task, dt):
        if getExpires(obj.panda3d_object) is not None:
            if getExpires(obj.panda3d_object) <= panda_task.time:
                interactions[1](obj, env)
        # may need to be changed to deal with a long vector of on and offs
        for i in self.update_list:
            behaviors[i](env, obj, panda_task, dt)
        updatePos(obj.panda3d_object, env, dt)

    def obj_interact(self, obj, env, other_obj):
        if other_obj is self:
            return
        p3do = other_obj.panda3d_object
        if ((obj.panda3d_object.getPos() - p3do.getPos()).lengthSquared() <
                (((obj.panda3d_object.getScale().getX() + p3do.getScale().getX())
                  * .5) ** 2)):
            oo_ident = other_obj.identifier
            interaction_list = np.where(np.array(self.interaction_dict[oo_ident]) > 0)[0]
            for i in interaction_list:
                interactions[i](obj, env)

    def dictionary(self):
        return {"static": False,
         "image_path": self.p3do_dict,
         "initializer": self.obj_initializer,
         "update": self.obj_behavior,
         "attributes": self.attributes,
         "interaction": self.obj_interact,
         "FSM": None
         }

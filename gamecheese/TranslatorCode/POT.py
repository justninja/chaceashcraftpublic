from TranslatorCode.HelperFunctions import *

from KnowledgeBase.rule_dict import interactions
from KnowledgeBase.rule_dict import controls
from KnowledgeBase.rule_dict import behaviors
from KnowledgeBase.rule_dict import locations

from panda3d.core import LVector3

import numpy as np


class POT:
    """
    Stands for Player Object Translator. Takes generated vectors and turns them into functions that can be passed into
    the GameApp class.
    """
    def __init__(self, identifier, generator_dict, used_locations_ptr):
        """
        Initialize the object.
        :param identifier:
        :param generator_dict:
        """
        self.generated = generator_dict
        self.identifier = identifier
        self.static = generator_dict['static']
        self.image_path = generator_dict['image_path']
        self.c_list = list(np.where(np.array(generator_dict['actions']) > 0)[0])
        self.b_list = list(np.where(np.array(generator_dict['update']) > 0)[0])
        self.attributes = generator_dict['attributes']
        self.interaction_dict = generator_dict['interaction']
        # self.interaction_list = np.where(np.array(generator_dict['interaction']) > 0)[0]
        self.FSM = generator_dict['FSM']
        self.x_func_idx = generator_dict['x_func']
        self.y_func_idx = generator_dict['y_func']

        self.used_locations = used_locations_ptr

        self.player_actions = {"arrow_left": ["left", 1],
                          "arrow_left-up": ["left", 0],
                          "arrow_right": ["right", 1],
                          "arrow_right-up": ["right", 0],
                          "arrow_up": ["up", 1],
                          "arrow_up-up": ["up", 0],
                          "arrow_down": ["down", 1],
                          "arrow_down-up": ["down", 0],
                          "w": ["w", 1],
                          "w-up": ["w", 0],
                          "a": ["a", 1],
                          "a-up": ["a", 0],
                          "s": ["s", 1],
                          "s-up": ["s", 0],
                          "d": ["d", 1],
                          "d-up": ["d", 0],
                          "space": ["space", 1],
                          "space-up": ["space", 0]
                          }

        self.attributes["initial_key_values"] = {"left": 0, "right": 0, "up": 0, "down": 0, "space": 0,
                                                 'w': 0, 'a': 0, 's': 0, 'd': 0}

    def player_initializer(self, env, player):
        loc_x = locations[self.x_func_idx](1)
        loc_y = locations[self.y_func_idx](1)
        self.used_locations.add((loc_x, loc_y))
        player.panda3d_object.setX(loc_x)
        player.panda3d_object.setZ(loc_y)
        setVelocity(player.panda3d_object, LVector3.zero())

    def player_behavior(self, obj, env, panda_task, dt):
        # may need to be changed to deal with a long vector of on and offs
        for i in self.c_list:
            controls[i](env, obj, panda_task, dt)
        for i in self.b_list:
            # print(i)
            behaviors[i](env, obj, panda_task, dt)
        updatePos(obj.panda3d_object, env, dt)

    def player_interact(self, player, env, other_obj):
        p3do = other_obj.panda3d_object
        if ((player.panda3d_object.getPos() - p3do.getPos()).lengthSquared() <
                (((player.panda3d_object.getScale().getX() + p3do.getScale().getX())
                  * .5) ** 2)):
            oo_ident = other_obj.identifier
            interaction_list = list(np.where(np.array(self.interaction_dict[oo_ident]) > 0)[0])
            for i in interaction_list:
                interactions[i](player, env)

    def dictionary(self):
        return {"image_path": "player.png",
                        "initializer": self.player_initializer,
                        "update": self.player_behavior,
                        "attributes": self.attributes,
                        "actions": self.player_actions,
                        "interaction": self.player_interact,
                        "FSM": None,
                        }

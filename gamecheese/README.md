Gamecheese was my project for Computational Creativity, done with Brian James and Huy Pham. The idea was to create a system
that would generate new video games from sets of possible video game rules. The system would invent the games and evaluate
them to produce better games. Not surprisingly, for a semester project, the results are lackluster, but it was a fun project
and I am mostly proud of what we did. The only regret I have about this project was some miscommunication early on about the
fitness function in the genetic algorithm. Currently it assesses simpler games as more fit, while I believe it should assess
more complex games as more fit, and let the phenotypic evaluator attempt to eliminate overly complex games.

This project was the brian child of Brian James. He developed the overall concept and wrote the rules for knowledge base. 
Huy Pham was responsible for the genetic algorithm and genotypic evaluation, and I wrote what I call the "framework" for 
the project as well as the phenotypic evaluator. All of the code is written in Python.

I included our final paper and presentation as references for our work, as well as Dr. Ventura's on which we based our 
creative system and the ANGELINA papers from which we heavily drew inspiration. 

Since this was a semester project, the code is messy in most places, and will likely not run as is. If anyone is
interested in me getting it to run on an arbitrary machine, please contact me at cc.ash.math@gmail.com and I will see what
I can do. The first thing to note is that the framework runs on Panda3D, an open source game engine developed for Disney and 
maintained by Carnegie-Mellon University. If you want to run this code, you will need Panda3D. 
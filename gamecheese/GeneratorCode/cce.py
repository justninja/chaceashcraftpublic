'''
Parent set (list of rules, objects, players)
Random score generator-representing aesthetic/fitness function
Co-evolution GA
'''

'''
Possible fitness
'''
import numpy as np
import random
import pickle
import sys
sys.path.append('../')

from sklearn.neighbors import KNeighborsRegressor
from KnowledgeBase.rule_dict import *

def convert_to_10(x):
    s = ''.join(str(bit) for bit in x)
    return int(s,2)
    
class Object: 
    def __init__(self, name, num_objects, num_rule_types, geno, attributes):
        self.object_type = name
        self.num_objects = num_objects
        self.num_rule_types = num_rule_types
        self.geno = geno
        self.attributes = attributes
        
    def printObject(self):
        print(self.object_type)
        print(self.num_objects)
        print(self.num_rule_types)
        print(self.geno)
        print(self.attributes)
        print(" ")
        
    def convertToDict(self, obj_id, num_type, player=False, controls=None):
        all_interactions = {}
        start = 0
        length = len(interactions)
        
        for i in range(num_type+1):
            if i != 0:
                all_interactions.update({"obj"+str(i):self.geno[start:start+length]})
            else:
                all_interactions.update({"player1":self.geno[start:start+length]}) 
            start += length
            
        behavior = np.zeros(16)
        behavior[convert_to_10(self.geno[start:])] = 1
            
        obj_dict = {self.object_type:{"static":False, "image_path": None,\
                                   "initializer":None, "init_num_objects": self.num_objects,\
                                   "update":behavior,"attributes": self.attributes,\
                                   "interaction":all_interactions, "FSM":None,\
                                   "x_func":np.random.randint(0,len(locations)),\
                                   "y_func":np.random.randint(0,len(locations))}}
        
        if player:
            obj_dict[self.object_type]["actions"] = controls
        
        return obj_dict
        
           
class Generator:
     
    
    def __init__(self):
        self.NUM_TYPE = np.random.randint(2,8)
        file = "KnowledgeBase/evaluated_games.pickle"
        data = pickle.load(open(file, 'rb'))
        if data:
            self.learn = True
            self.trainKNN(data)
    
    
    def findFirst(self, x, a):
        for i in range(len(x)):
            if x[i] == a:
                return i
        return -1
    
    
    def mergeGeno(self, item, pad_size):
        interaction = []
        
        for key, value in item["interaction"].items():
            interaction = np.hstack((interaction,value))
        
        for i in range(pad_size):
            zero = np.zeros(len(interactions))
            interaction = np.hstack((interaction,zero))
        
        behavior = []
        binary = format(self.findFirst(item["update"], 1),"04b")
        for i in range(len(binary)):
            behavior.append(int(binary[i]))
        
        species = np.hstack((interaction,behavior))
        
        return species
    
    
    def mergeControl(self, controls):
        control0 = controls[0:7]
        control1 = controls[7:14]
        control2 = controls[14:31]
        
        num = self.findFirst(control0, 1)
        if num == -1:
            return []
        bin_control0 = format(num,"03b")
        control0 = []
        for i in range(len(bin_control0)):
            control0.append(int(bin_control0[i]))
        
        num = self.findFirst(control1, 1)
        if num == -1:
            return []
        bin_control1 = format(num,"03b")
        control1 = []
        for i in range(len(bin_control1)):
            control1.append(int(bin_control1[i]))

        num = self.findFirst(control2, 1)
        if num == -1:
            return []
        bin_control2 = format(num,"05b")
        control2 = []
        for i in range(len(bin_control2)):
            control2.append(int(bin_control2[i]))        
        
        new_control = np.hstack((control0, control1, control2))
        
        return new_control
    
    
    def trainKNN(self, data):
        trainings = []
        targets = []
        count = 0
        for item in data:
            #print(item)
            game = []
            num = 8 - len(item["game_objects"])
            
            species = self.mergeGeno(item["player1"], num)
            game = np.hstack((species))
            
            for key, value in item["game_objects"].items():
                species = self.mergeGeno(value, num)
                game = np.hstack((game, species))
            
            species = np.zeros((len(interactions)*9 + 4)*num)
            game = np.hstack((game, species))
            
            controls = self.mergeControl(item["player1"]["actions"])
            
            if not list(controls):
                count += 1
                continue
            
            game = np.hstack((game, controls, item["win"],item["lose"]))
            
            trainings.append(game)
            if "H_SCORE" in item:
                targets.append(item["H_SCORE"])
            else:
                targets.append(item["PE_SCORE"])
        
        self.learner = KNeighborsRegressor(3, "distance")
        self.learner.fit(trainings, targets)
        #print(count)
        
    def getGame(self):
        self.game = []
        object_list = {}
        
        num_species = self.NUM_TYPE + 6
        score = 0
        while score == 0:
            geno, score = self.generate(num_species)
        
        max_hp = np.random.randint(1,10)
        hp = max_hp+1
        while hp > max_hp:
            hp = np.random.randint(1,6)
        
        attributes = {"alive": True, "MAX_VEL": np.random.randint(1,6),\
                      "HP": hp, "MAX_HP": max_hp,\
                      "initial_key_values": {"up": 0, "down": 0, "left": 0, "right": 0, "space": 0}}
        
        self.game.append(Object("player1", 1, 3, geno[0], attributes))

        for i in range(1, self.NUM_TYPE+1):
            num_objects = np.random.randint(1,10)
            
            max_vel = np.random.randint(1,6)
            int_vel = max_vel+1
            while int_vel > max_vel:
                int_vel = np.random.randint(1,6)
                
            max_hp = np.random.randint(1,10)
            hp = max_hp+1
            while hp > max_hp:
                hp = np.random.randint(1,6)
                
            attributes = {"static": False, "INIT_VEL": int_vel, "MAX_VEL": max_vel,\
                          "HP": hp, "MAX_HP": max_hp}
            self.game.append(Object("obj"+str(i), num_objects, 3, geno[i], attributes))
            
            control_geno = np.zeros((31,), dtype=int)
            control_geno[convert_to_10(geno[num_species-5])] = 1
            control_geno[convert_to_10(geno[num_species-4])+7] = 1
            control_geno[convert_to_10(geno[num_species-3])+14] = 1 
                                       
        object_list.update(self.game[0].convertToDict(0, self.NUM_TYPE, True,\
                           control_geno))
        object_list["game_objects"] = {}
        
        for i in range(1, self.NUM_TYPE+1):
            object_list["game_objects"].update(self.game[i].convertToDict(i, self.NUM_TYPE))
        
        object_list["win"] = geno[num_species-2]
        object_list["lose"] = geno[num_species-1]
        
        #print(object_list)

        return object_list
    
    
    def total_fitness(self, rep, num_species):
        if self.learn and np.random.rand() < 0.6:
            game = []
            inter_size = len(interactions)
            num = self.NUM_TYPE+1
            
            for i in range(num):
                game = np.hstack((game,rep[i][0:(inter_size * (num))]))
                for j in range(8-self.NUM_TYPE):
                    zero = np.zeros(inter_size)
                    game = np.hstack((game,zero))
                game = np.hstack((game,rep[i][(inter_size * (num)):]))
            
            for i in range(8-self.NUM_TYPE):
                zero = np.zeros(inter_size*9+4)
                game = np.hstack((game,zero))
            
            for i in range(5):
                game = np.hstack((game,rep[num+i]))
            
            score = self.learner.predict(np.reshape(game,(1,-1)))
            
            score = score[0]
        else:
            score = 0
            for i in range(num_species):
                score += (rep[i] == 1).sum()
        
        #Arrow keys conflicts
        num = convert_to_10(rep[num_species - 5])
        if num > 6:
            return 0
        
        #wasd keys conflicts
        num = convert_to_10(rep[num_species - 4])
        if num > 6:
            return 0
        
        #keys conflicts
        num = convert_to_10(rep[num_species - 3])
        if num > 16:
            return 0
        
        return score

    def cross_over(self, x):
        new_x = []
        for i in range(5):
            x1 = 0
            x2 = 0
            while x1 == x2:
                x1 = np.random.randint(len(x))
                x2 = np.random.randint(len(x))
    
            position = np.random.randint(len(x[x1]))
            start_x1 = x[x1, :position]
            end_x2 = x[x1, position:]
            start_x2 = x[x2, :position]
            end_x1 = x[x2, position:]
            new_x1 = np.concatenate([start_x1, end_x1])
            new_x2 = np.concatenate([start_x2, end_x2])
            new_x.append(new_x1)
            new_x.append(new_x2)
        return np.array(new_x)	
    
    
    def mutate(self, x):
        for row in x:
            pos1 = np.random.randint(len(row))
            pos2 = np.random.randint(len(row))
            row[pos1] = np.absolute(row[pos1] - 1)
            row[pos2] = np.absolute(row[pos2] - 1)
        return x
    
    
    
    def select(self, x, scores):
        temp_scores = []
        count = 0
        temp_list = []
        while count < 4:
            index = np.argmax(scores)
            temp_list.append(x[index])
            temp_scores.append(scores[index])
            np.delete(x,index)
            np.delete(scores, index)
            count += 1
    
        x = np.array(temp_list)
        scores = temp_scores
        return x, scores
    
    
    def generate(self, num_species):
        size = len(interactions) * (self.NUM_TYPE+1) + 4 #interaction with player and n objects
        arrow_controls_size = 3
        wasd_controls_size = 3
        space_controls_size = 5
        win_size = len(win_objectives)
        lose_size = len(lose_objectives)
        species = []
        
        for _ in range(num_species-5):
            parents = [] 
            parents.append(np.random.randint(2, size=size)) 
            parents.append(np.random.randint(2, size=size))
            species.append(np.array(parents))
         
        #arrow
        parents = [] 
        parents.append(np.random.randint(2, size=arrow_controls_size))
        parents.append(np.random.randint(2, size=arrow_controls_size))
        species.append(np.array(parents))
        
        #wasd
        parents = [] 
        parents.append(np.random.randint(2, size=wasd_controls_size))
        parents.append(np.random.randint(2, size=wasd_controls_size))
        species.append(np.array(parents))
        
        #space
        parents = [] 
        parents.append(np.random.randint(2, size=space_controls_size))
        parents.append(np.random.randint(2, size=space_controls_size))
        species.append(np.array(parents))
        
        parents = [] 
        parents.append(np.random.randint(2, size=win_size))
        parents.append(np.random.randint(2, size=win_size))
        species.append(np.array(parents))

        parents = [] 
        parents.append(np.random.randint(2, size=lose_size))
        parents.append(np.random.randint(2, size=lose_size))
        species.append(np.array(parents))
        
        rep = [random.choice(species[i]) for i in range(num_species)]
        
        target_max = 0
        target_rep = rep
        
        for _ in range(1000):
            # Initialize a container for the next generation rep
            next_rep = [None] * num_species
            for (i, s) in enumerate(species):
                # Vary the species individuals
                new_children = self.cross_over(s)
                s = self.mutate(new_children)
                
                ind_fitness_values = []
                for ind in s:
                    # Get the new rep with the current species
                    r = rep[:i] + [ind] + rep[i+1:]
                    # Evaluate and set the individual fitness
                    ind_fitness_values.append(self.total_fitness(r, num_species))
                    
                # Select the individuals
                species[i], ind_fitness_values = self.select(s, ind_fitness_values)  # Tournament selection
                next_rep[i] = s[np.argmax(ind_fitness_values)]# Best selection
                
                
            max_fitness = self.total_fitness(next_rep, num_species)
            if max_fitness > target_max:
                target_rep = next_rep
                target_max = max_fitness
                #print(target_max)
                
            rep = next_rep
        return target_rep, target_max       
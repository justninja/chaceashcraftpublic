"""
File containing the relevant data for the asteroids game. First set up the functions and variables used
to define the parameters passed into the GameApp object, then define the parameters.
"""

from panda3d.core import LVector3
from direct.interval.MetaInterval import Sequence
from direct.interval.FunctionInterval import Wait, Func
from direct.task.Task import Task

from GameApp.GameObject import GameObject
from GameApp.DynGameObjectInst import DynGameObjectInst
from GameApp.faux_generated_objects import Player, game_objects, spawn_asteroids
from GameApp.Environment import Environment

from math import pi, sin, cos
from random import randint, choice, random
import sys

import GameApp.faux_generated_objects as generated

# environment functions and Variables

environment_attributes = {"SCREEN_X": 20,
                          "SCREEN_Y": 15,
                          "initial_key_values": {"exit": 0}
                          }

environment_actions = {"escape": ["exit", 1]}


def environment_initializer(env):
    pass


def environment_update(env, panda_task, dt):
    if env.keys['exit']:
        sys.exit()

    if len(env.GameObjects['asteroid'].container) == 0:
        spawn_asteroids(env, env.GameObjects['asteroid'])

    return Task.cont  # Since every return is Task.cont, the task will
    # continue indefinitely


def environment_interaction(env, panda_task, dt):
    pass


# Players Functions and Variables

player_attributes = {"alive": True,
                     "TURN_RATE": 360,
                     "DEG_TO_RAD": pi/180,
                     "ACCELERATION": 10,
                     "MAX_VEL_SQ": 6**2,
                     "MAX_VEL": 6,
                     "initial_key_values": {"left": 0, "right": 0, "up": 0, "down": 0, "space": 0}
                     }

player_actions = {"arrow_left": ["left", 1],
                  "arrow_left-up": ["left", 0],
                  "arrow_right": ["right", 1],
                  "arrow_right-up": ["right", 0],
                  "arrow_up": ["up", 1],
                  "arrow_up-up": ["up", 0],
                  "arrow_down": ["down", 1],
                  "arrow_down-up": ["down", 0],
                  "space": ["space", 1],
                  "space-up": ["space", 0]
                  }


def setVelocity(obj, val):
    obj.setPythonTag("velocity", val)

def getVelocity(obj):
    return obj.getPythonTag("velocity")


def setExpires(obj, val):
    obj.setPythonTag("expires", val)


def getExpires(obj):
    return obj.getPythonTag("expires")

# Updates the positions of objects
def updatePos(obj, env, dt):
    vel = getVelocity(obj)
    newPos = obj.getPos() + (vel * dt)

    # Check if the object is out of bounds. If so, wrap it
    radius = .5 * obj.getScale().getX()
    if newPos.getX() - radius > env.attributes["SCREEN_X"]:
        newPos.setX(-env.attributes["SCREEN_X"])
    elif newPos.getX() + radius < -env.attributes["SCREEN_X"]:
        newPos.setX(env.attributes["SCREEN_X"])
    if newPos.getZ() - radius > env.attributes["SCREEN_Y"]:
        newPos.setZ(-env.attributes["SCREEN_Y"])
    elif newPos.getZ() + radius < -env.attributes["SCREEN_Y"]:
        newPos.setZ(env.attributes["SCREEN_Y"])

    obj.setPos(newPos)


def setAttribute(obj, key, new_value):
    obj[key] = new_value


def restart_game(ship, ast, env):
        env.Players['player1'].attributes['alive'] = False  # Ship is no longer alive
        # Remove every object in asteroids and bullets from the scene
        for i in (list(ast.parent.container)
                  + list(env.GameObjects['bullet'].container)):
            i.remove_self()
        # env.GameObjects["bullet"].container.clear()  # Clear the bullet list
        # ast.parent.container.clear()
        ship.hide()  # Hide the ship
        # Re-initialize the initial key values, if needed -- not needed in this case
        # for set_key, in_val in Environment.Players['player1'].attributes['initial_key_values'].items():
        #     Environment.keys[set_key] = in_val
        # Reset the velocity
        setVelocity(ship, LVector3(0, 0, 0))
        Sequence(Wait(2),  # Wait 2 seconds
                 Func(ship.setR, 0),  # Reset heading
                 Func(ship.setX, 0),  # Reset position X
                 # Reset position Y (Z for Panda)
                 Func(ship.setZ, 0),
                 Func(ship.show),  # Show the ship
                 Func(spawn_asteroids, *[env, ast.parent])).start()  # Remake asteroids
        return Task.cont


def player_update(player, env, panda_task, dt):
    if not player.attributes['alive']:
        return Task.cont
    # generated.controls[0](env, player, panda_task, dt)
    generated.check_keys(env, player, panda_task, dt)
    # Finally, update the position as with any other object
    updatePos(player.panda3d_object, env, dt)


def player_initializer(env, player):
    setVelocity(player.panda3d_object, LVector3.zero())


def player_interaction(playerOb, other_object, env, panda_task, dt):
    if other_object.identifier == "asteroid":
        ast = other_object
        ship = playerOb.panda3d_object

        shipSize = ship.getScale().getX()
        if ((ship.getPos() - ast.panda3d_object.getPos()).lengthSquared() <
                (((shipSize + ast.panda3d_object.getScale().getX()) * .5) ** 2)):
            # If there is a hit, clear the screen and schedule a restart
            env.stack_interaction(restart_game, [ship, ast, env])

# Objects functions and variables


# Creates a bullet and adds it to the bullet list
def fire(bulletGameOb, env, time):
    ship = env.Players['player1'].panda3d_object
    direction = bulletGameOb.attributes["DEG_TO_RAD"] * ship.getR()
    pos = ship.getPos()
    bullet = DynGameObjectInst(bulletGameOb, env)
    bullet.panda3d_object.setPos(pos)
    # Velocity is in relation to the ship
    vel = (getVelocity(ship) +
           (LVector3(sin(direction), 0, cos(direction)) *
            bulletGameOb.attributes['BULLET_SPEED']))
    setVelocity(bullet.panda3d_object, vel)
    # Set the bullet expiration time to be a certain amount past the
    # current time
    setExpires(bullet.panda3d_object, time + bulletGameOb.attributes['BULLET_LIFE'])

    # Finally, add the new bullet to the list
    env.stack_interaction(bulletGameOb.container.add, [bullet])





# Parameters used by the GameApp object vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

game_attributes = {"set_background_color": (0, 0, 0, 1),
                   "bg": {"tex": "stars.jpg", "scale": 146, "depth": 200, "transparency": False},  # "antistars.png"
                   # provides a white background with black spots, if we would rather that
                   "camera.setPosHpr": [0, 0, 25, 0, -90, 0]
                   }

players = {"player1":
               {"image_path": "ship.png",
                "initializer": player_initializer,
                "update": player_update,
                "attributes": player_attributes,
                "actions": player_actions,
                "interaction": player_interaction,
                "FSM": None,
                }
           }

environment = {"initializer": environment_initializer,
               "attributes": environment_attributes,
               "players": players,
               "game_objects": game_objects,
               "actions": environment_actions,
               "interaction": environment_interaction,
               "update": environment_update,
               "FSM": None}
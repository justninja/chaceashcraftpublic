import copy


class DynGameObjectInst:
    """
    Class representing the general idea of an NPC in our games using Panda3D, that is an instance of a dynamic
    GameObject.
    """

    def __init__(self, parent, environment, panda3d_object=None):
        """
        An dynamic GameObject. Should be the same as a GameObject object, but represents an instance of a single
            dynamic object, rather than a regular GameObject that can carry instances of itself.
        :param parent: (GameObject) The parent GameObject to this one.
        :param environment: (Environment) Reference to the Environment to create the panda3d_object.
        """
        self.parent = parent
        self.attributes = copy.copy(parent.attributes)
        self.identifier = parent.identifier
        self.panda3d_object = panda3d_object
        if panda3d_object is None:
            self.panda3d_object = environment.gameApp.loadObject(**parent.panda3d_object)
        self.interaction = parent.interaction
        self.update_func = parent.update_func

        self.removed = False

    def update(self, environment, panda_task, dt):
        if self.update_func is not None:
            return self.update_func(self, environment, panda_task, dt)

    def interact(self, other_object, environment, panda_task, dt):
        if self.interaction is not None:
            return self.interaction(self, environment, other_object)
            #(self, other_object, environment, panda_task, dt)

    def remove_self(self):
        self.removed = True
        self.panda3d_object.removeNode()
        self.parent.container.remove(self)

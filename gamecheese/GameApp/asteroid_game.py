"""
File containing the relevant data for the asteroids game. First set up the functions and variables used
to define the parameters passed into the GameApp object, then define the parameters.
"""

from panda3d.core import LVector3
from direct.interval.MetaInterval import Sequence
from direct.interval.FunctionInterval import Wait, Func
from direct.task.Task import Task
from math import pi, sin, cos
from random import randint, choice, random
import sys

# Environment functions and Variables

environment_attributes = {"SCREEN_X": 20,
                          "SCREEN_Y": 15,
                          "initial_key_values": {"exit": 0}
                          }

environment_actions = {"escape": ["exit", 1]}


def environment_initializer(Environment):
    pass


def environment_update(Environment, panda_task, dt):
    if Environment.keys['exit']:
        sys.exit()

    return Task.cont  # Since every return is Task.cont, the task will
    # continue indefinitely


def environment_interactions(Environment, panda_task, dt):
    pass

# Players Functions and Variables

player_attributes = {"alive": True,
                     "TURN_RATE": 360,
                     "DEG_TO_RAD": pi/180,
                     "ACCELERATION": 10,
                     "MAX_VEL_SQ": 6**2,
                     "MAX_VEL": 6,
                     "initial_key_values": {"turnLeft": 0, "turnRight": 0, "accel": 0, "fire": 0}
                     }

player_actions = {"arrow_left": ["turnLeft", 1],
                  "arrow_left-up": ["turnLeft", 0],
                  "arrow_right": ["turnRight", 1],
                  "arrow_right-up": ["turnRight", 0],
                  "arrow_up": ["accel", 1],
                  "arrow_up-up": ["accel", 0],
                  "space": ["fire", 1]
                  }


def setVelocity(obj, val):
    obj.setPythonTag("velocity", val)


def getVelocity(obj):
    return obj.getPythonTag("velocity")


def setExpires(obj, val):
    obj.setPythonTag("expires", val)


def getExpires(obj):
    return obj.getPythonTag("expires")


# Updates the positions of objects
def updatePos(obj, Environment, dt):
    vel = getVelocity(obj)
    newPos = obj.getPos() + (vel * dt)

    # Check if the object is out of bounds. If so, wrap it
    radius = .5 * obj.getScale().getX()
    if newPos.getX() - radius > Environment.attributes["SCREEN_X"]:
        newPos.setX(-Environment.attributes["SCREEN_X"])
    elif newPos.getX() + radius < -Environment.attributes["SCREEN_X"]:
        newPos.setX(Environment.attributes["SCREEN_X"])
    if newPos.getZ() - radius > Environment.attributes["SCREEN_Y"]:
        newPos.setZ(-Environment.attributes["SCREEN_Y"])
    elif newPos.getZ() + radius < -Environment.attributes["SCREEN_Y"]:
        newPos.setZ(Environment.attributes["SCREEN_Y"])

    obj.setPos(newPos)


def player_update(Player, Environment, panda_task, dt):
    if not Player.attributes['alive']:
        return Task.cont

    keys = Environment.keys
    heading = Player.panda3d_object.getR()  # Heading is the roll value for this model
    # Change heading if left or right is being pressed
    if keys["turnRight"]:
        heading += dt * Player.attributes["TURN_RATE"]
        Player.panda3d_object.setR(heading % 360)
    elif keys["turnLeft"]:
        heading -= dt * Player.attributes["TURN_RATE"]
        Player.panda3d_object.setR(heading % 360)

    # Thrust causes acceleration in the direction the ship is currently
    # facing
    if keys["accel"]:
        heading_rad = Player.attributes["DEG_TO_RAD"] * heading

        # This builds a new velocity vector and adds it to the current one
        # relative to the camera, the screen in Panda is the XZ plane.
        # Therefore all of our Y values in our velocities are 0 to signify
        # no change in that direction.
        newVel = \
            LVector3(sin(heading_rad), 0, cos(heading_rad)) * Player.attributes["ACCELERATION"] * dt
        newVel += getVelocity(Player.panda3d_object)

        # Clamps the new velocity to the maximum speed. lengthSquared() is
        # used again since it is faster than length()
        if newVel.lengthSquared() > Player.attributes["MAX_VEL_SQ"]:
            newVel.normalize()
            newVel *= Player.attributes["MAX_VEL"]
        setVelocity(Player.panda3d_object, newVel)

    # Finally, update the position as with any other object
    updatePos(Player.panda3d_object, Environment, dt)


def player_initializer(Environment, player):
    setVelocity(player.panda3d_object, LVector3.zero())


def player_interactions(playerGameOb, Environment, panda_task, dt):
    pass


# Objects functions and variables

bullet_attributes = {"static": False,  # this attribute should be in all objects, just in case it is useful
                     "DEG_TO_RAD": pi / 180,
                     "BULLET_SPEED": 10,
                     "BULLET_LIFE": 2,
                     "BULLET_REPEAT": 0.2,
                     "nextBullet": 0.0,
                     "bullets": []
                     }

bullet_panda3d_object = {"tex": "bullet.png", 'scale': 0.2}


def bullet_initializer(Environment, bulletGameOb):
    bulletGameOb.attributes['bullets'] = []  # not really needed, but conveys idea


# Creates a bullet and adds it to the bullet list
def fire(bulletGameOb, Environment, time):
    ship = Environment.Players['player1'].panda3d_object
    direction = bulletGameOb.attributes["DEG_TO_RAD"] * ship.getR()
    pos = ship.getPos()
    bullet = Environment.gameApp.loadObject(**bulletGameOb.panda3d_object)  # Create the object
    bullet.setPos(pos)
    # Velocity is in relation to the ship
    vel = (getVelocity(ship) +
           (LVector3(sin(direction), 0, cos(direction)) *
            bulletGameOb.attributes['BULLET_SPEED']))
    setVelocity(bullet, vel)
    # Set the bullet expiration time to be a certain amount past the
    # current time
    setExpires(bullet, time + bulletGameOb.attributes['BULLET_LIFE'])

    # Finally, add the new bullet to the list
    bulletGameOb.attributes['bullets'].append(bullet)


def bullet_update(bulletGameOb, Environment, panda_task, dt):
    # check to see if the ship can fire
    if Environment.keys["fire"] and panda_task.time > bulletGameOb.attributes['nextBullet']:
        fire(bulletGameOb, Environment, panda_task.time)  # If so, call the fire function
        # And disable firing for a bit
        bulletGameOb.attributes['nextBullet'] = panda_task.time + bulletGameOb.attributes['BULLET_REPEAT']
    # Remove the fire flag until the next spacebar press
    Environment.keys["fire"] = 0

    # update bullets
    newBulletArray = []
    for obj in bulletGameOb.attributes["bullets"]:
        updatePos(obj, Environment, dt)  # Update the bullet
        # Bullets have an expiration time (see definition of fire)
        # If a bullet has not expired, add it to the new bullet list so
        # that it will continue to exist.
        if getExpires(obj) > panda_task.time:
            newBulletArray.append(obj)
        else:
            obj.removeNode()  # Otherwise, remove it from the scene.
    # Set the bullet array to be the newly updated array
    bulletGameOb.attributes["bullets"] = newBulletArray


def bullet_interactions(bulletGameOb, Environment, panda_task, dt):
    pass


asteroid_attributes = {"static": False,
                        "asteroids": [],
                        "AST_INIT_SCALE": 3,
                        "AST_INIT_VEL": 1,
                        "AST_VEL_SCALE": 2.2,
                        "AST_SIZE_SCALE": 0.6,
                        "AST_MIN_SCALE": 1.1
                        }

asteroid_panda3d_object = {'tex': "asteroid%d.png" % (randint(1, 3)), 'scale': asteroid_attributes['AST_INIT_SCALE']}


# The handler when an asteroid is hit by a bullet
def asteroidHit(asteroidGameOb, Environment, index):
    # If the asteroid is small it is simply removed
    if asteroidGameOb.attributes['asteroids'][index].getScale().getX() <= asteroidGameOb.attributes['AST_MIN_SCALE']:
        asteroidGameOb.attributes['asteroids'][index].removeNode()
        # Remove the asteroid from the list of asteroids.
        del asteroidGameOb.attributes['asteroids'][index]
    else:
        # If it is big enough, divide it up into little asteroids.
        # First we update the current asteroid.
        asteroid = asteroidGameOb.attributes['asteroids'][index]
        newScale = asteroid.getScale().getX() * asteroidGameOb.attributes['AST_SIZE_SCALE']
        asteroid.setScale(newScale)  # Rescale it

        # The new direction is chosen as perpendicular to the old direction
        # This is determined using the cross product, which returns a
        # vector perpendicular to the two input vectors.  By crossing
        # velocity with a vector that goes into the screen, we get a vector
        # that is orthagonal to the original velocity in the screen plane.
        vel = getVelocity(asteroid)
        speed = vel.length() * asteroidGameOb.attributes['AST_VEL_SCALE']
        vel.normalize()
        vel = LVector3(0, 1, 0).cross(vel)
        vel *= speed
        setVelocity(asteroid, vel)

        # Now we create a new asteroid identical to the current one
        newAst = Environment.gameApp.loadObject(asteroidGameOb.panda3d_object['tex'], scale=newScale)
        setVelocity(newAst, vel * -1)
        newAst.setPos(asteroid.getPos())
        newAst.setTexture(asteroid.getTexture(), 1)
        asteroidGameOb.attributes['asteroids'].append(newAst)


def spawn_asteroids(Environment, asteroidGameOb):  # spawnAsteroids() function in asteroids.py sample file
    # Control variable for if the ship is alive
    Environment.Players['player1'].attributes['alive'] = True
    asteroidGameOb.attributes['asteroids'] = []  # List that will contain our asteroids

    for i in range(10):
        # This loads an asteroid. The texture chosen is random
        # from "asteroid1.png" to "asteroid3.png".
        asteroidGameOb.panda3d_object['tex'] = "asteroid%d.png" % (randint(1, 3))  # hack to get randomness...
        asteroid = Environment.gameApp.loadObject(**asteroidGameOb.panda3d_object)
        asteroidGameOb.attributes['asteroids'].append(asteroid)

        # This is kind of a hack, but it keeps the asteroids from spawning
        # near the player.  It creates the list (-20, -19 ... -5, 5, 6, 7,
        # ... 20) and chooses a value from it. Since the player starts at 0
        # and this list doesn't contain anything from -4 to 4, it won't be
        # close to the player.
        asteroid.setX(choice(tuple(range(-Environment.attributes['SCREEN_X'], -5))
                             + tuple(range(5, Environment.attributes['SCREEN_X']))))
        # Same thing for Y
        asteroid.setZ(choice(tuple(range(-Environment.attributes['SCREEN_Y'], -5))
                             + tuple(range(5, Environment.attributes['SCREEN_Y']))))

        # Heading is a random angle in radians
        heading = random() * 2 * pi

        # Converts the heading to a vector and multiplies it by speed to
        # get a velocity vector
        v = LVector3(sin(heading), 0, cos(heading)) * asteroidGameOb.attributes['AST_INIT_VEL']
        setVelocity(asteroidGameOb.attributes['asteroids'][i], v)


def asteroid_update(asteroidGameOb, Environment, panda_task, dt):
    for obj in asteroidGameOb.attributes['asteroids']:
        updatePos(obj, Environment, dt)


def asteroid_interactions(asteroidGameOb, Environment, panda_task, dt):
    # Check bullet collision with asteroids
    # In short, it checks every bullet against every asteroid. This is
    # quite slow.  A big optimization would be to sort the objects left to
    # right and check only if they overlap.  Framerate can go way down if
    # there are many bullets on screen, but for the most part it's okay.
    bullets = Environment.GameObjects["bullet"].attributes['bullets']
    for bullet in bullets:
        # This range statement makes it step though the asteroid list
        # backwards.  This is because if an asteroid is removed, the
        # elements after it will change position in the list.  If you go
        # backwards, the length stays constant.
        for i in range(len(asteroidGameOb.attributes['asteroids']) - 1, -1, -1):
            asteroid = asteroidGameOb.attributes['asteroids'][i]
            # Panda's collision detection is more complicated than we need
            # here.  This is the basic sphere collision check. If the
            # distance between the object centers is less than sum of the
            # radii of the two objects, then we have a collision. We use
            # lengthSquared() since it is faster than length().
            if ((bullet.getPos() - asteroid.getPos()).lengthSquared() <
                    (((bullet.getScale().getX() + asteroid.getScale().getX())
                      * .5) ** 2)):
                # Schedule the bullet for removal
                setExpires(bullet, 0)
                asteroidHit(asteroidGameOb, Environment, i)  # Handle the hit

    # Now we do the same collision pass for the ship
    ship = Environment.Players['player1'].panda3d_object
    shipSize = ship.getScale().getX()
    for ast in asteroidGameOb.attributes['asteroids']:
        # Same sphere collision check for the ship vs. the asteroid
        if ((ship.getPos() - ast.getPos()).lengthSquared() <
                (((shipSize + ast.getScale().getX()) * .5) ** 2)):
            # If there is a hit, clear the screen and schedule a restart
            Environment.Players['player1'].attributes['alive'] = False  # Ship is no longer alive
            # Remove every object in asteroids and bullets from the scene
            for i in asteroidGameOb.attributes['asteroids'] + bullets:
                i.removeNode()
            Environment.GameObjects["bullet"].attributes['bullets'] = []  # Clear the bullet list
            ship.hide()  # Hide the ship
            # Re-initialize the initial key values, if needed -- not needed in this case
            # for set_key, in_val in Environment.Players['player1'].attributes['initial_key_values'].items():
            #     Environment.keys[set_key] = in_val
            # Reset the velocity
            setVelocity(ship, LVector3(0, 0, 0))
            Sequence(Wait(2),  # Wait 2 seconds
                     Func(ship.setR, 0),  # Reset heading
                     Func(ship.setX, 0),  # Reset position X
                     # Reset position Y (Z for Panda)
                     Func(ship.setZ, 0),
                     Func(ship.show),  # Show the ship
                     Func(spawn_asteroids, *[Environment, asteroidGameOb])).start()  # Remake asteroids
            return Task.cont

    # If the player has successfully destroyed all asteroids, respawn them
    if len(asteroidGameOb.attributes['asteroids']) == 0:
        spawn_asteroids(Environment, asteroidGameOb)


# Parameters used by the GameApp object vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

game_attributes = {"set_background_color": (0, 0, 0, 1),
                   "bg": {"tex": "stars.jpg", "scale": 146, "depth": 200, "transparency": False},  # "antistars.png"
                   # provides a white background with black spots, if we would rather that
                   "camera.setPosHpr": [0, 0, 25, 0, -90, 0]
                   }

players = {"player1":
               {"image_path": "ship.png",
                "initializer": player_initializer,
                "update": player_update,
                "attributes": player_attributes,
                "actions": player_actions,
                "interactions": player_interactions,
                "FSM": None,
                }
           }

game_objects = {"bullet":
                    {"static": False,
                     "image_path": bullet_panda3d_object,
                     "initializer": bullet_initializer,
                     "update": bullet_update,
                     "attributes": bullet_attributes,
                     "interactions": bullet_interactions,
                     "FSM": None
                     },
                "asteroid":
                    {"static": False,
                     "image_path": asteroid_panda3d_object,
                     "initializer": spawn_asteroids,
                     "update": asteroid_update,
                     "attributes": asteroid_attributes,
                     "interactions": asteroid_interactions,
                     "FSM": None
                     }
                }

environment = {"initializer": environment_initializer,
               "attributes": environment_attributes,
               "players": players,
               "game_objects": game_objects,
               "actions": environment_actions,
               "interactions": environment_interactions,
               "update": environment_update,
               "FSM": None}

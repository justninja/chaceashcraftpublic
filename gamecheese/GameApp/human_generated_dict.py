from numpy import array

dict = {
    'player1': {
        'static': False,
        'image_path': None,
        'initializer': None,
        'init_num_objects': 1,
        'update': array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
        'attributes': {
            'alive': True,
            'MAX_VEL': 5,
            'HP': 3,
            'MAX_HP': 4,
            'initial_key_values': {'up': 0, 'down': 0, 'left': 0, 'right': 0, 'space': 0}
        },
        'interaction': {
            'player1': array([1, 0, 0, 0, 0, 0, 0, 0, 0]),
            'obj1': array([1, 0, 0, 0, 0, 1, 0, 1, 1]),
            'obj2': array([0, 1, 0, 0, 0, 0, 0, 0, 0])
        },
        'FSM': None,
        'x_func': 22,
        'y_func': 49,
        'actions': array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    },
    'game_objects': {
        'obj1': {
            'static': False,
            'image_path': None,
            'initializer': None,
            'init_num_objects': 0,
            'update': array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]),
            'attributes': {
                'static': False,
                'INIT_VEL': 3,
                'MAX_VEL': 3,
                'HP': 1,
                'MAX_HP': 1
            },
            'interaction': {
                'player1': array([1, 0, 0, 0, 0, 0, 0, 0, 0]),
                'obj1': array([1, 0, 1, 0, 0, 0, 0, 0, 0]),
                'obj2': array([0, 1, 1, 0, 0, 0, 0, 0, 0])
            }, 'FSM': None,
            'x_func': 28,
            'y_func': 84
        },
        'obj2': {
            'static': False,
            'image_path': None,
            'initializer': None,
            'init_num_objects': 5,
            'update': array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]),
            'attributes': {
                'static': False,
                'INIT_VEL': 2,
                'MAX_VEL': 2,
                'HP': 1,
                'MAX_HP': 5
            },
            'interaction': {
                'player1': array([1, 0, 0, 0, 0, 0, 0, 0, 0]),
                'obj1': array([0, 1, 0, 0, 0, 0, 0, 0, 0]),
                'obj2': array([1, 0, 0, 0, 0, 0, 0, 0, 0])
            },
            'FSM': None,
            'x_func': 72,
            'y_func': 6}
    },
    'win': array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
    'lose': array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
}

g1 = {'player1': {'static': False, 'image_path': None, 'initializer': None, 'init_num_objects': 1, 'update': array([1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1]), 'attributes': {'alive': True, 'MAX_VEL': 3, 'HP': 4, 'MAX_HP': 4, 'initial_key_values': {'up': 0, 'down': 0, 'left': 0, 'right': 0, 'space': 0}}, 'interaction': {'player1': array([1, 0, 0, 0, 0, 1, 0, 0, 1]), 'obj1': array([0, 0, 0, 0, 0, 1, 0, 1, 0]), 'obj2': array([0, 1, 1, 0, 0, 1, 0, 0, 1]), 'obj3': array([0, 0, 0, 0, 1, 1, 0, 1, 0]), 'obj4': array([1, 0, 0, 0, 0, 1, 0, 0, 1])}, 'FSM': None, 'x_func': 79, 'y_func': 39, 'actions': array([1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
       0, 0, 0, 0, 0, 0, 0, 0])}, 'game_objects': {'obj1': {'static': False, 'image_path': None, 'initializer': None, 'init_num_objects': 9, 'update': array([1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1]), 'attributes': {'static': False, 'INIT_VEL': 2, 'MAX_VEL': 2, 'HP': 1, 'MAX_HP': 3}, 'interaction': {'player1': array([1, 1, 0, 0, 0, 1, 1, 0, 0]), 'obj1': array([0, 1, 1, 1, 0, 0, 1, 1, 1]), 'obj2': array([0, 1, 0, 0, 1, 0, 1, 1, 1]), 'obj3': array([1, 1, 0, 1, 1, 0, 0, 0, 0]), 'obj4': array([1, 1, 1, 1, 0, 1, 1, 0, 1])}, 'FSM': None, 'x_func': 34, 'y_func': 70}, 'obj2': {'static': False, 'image_path': None, 'initializer': None, 'init_num_objects': 4, 'update': array([0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1]), 'attributes': {'static': False, 'INIT_VEL': 3, 'MAX_VEL': 4, 'HP': 4, 'MAX_HP': 8}, 'interaction': {'player1': array([1, 0, 0, 1, 0, 1, 1, 0, 0]), 'obj1': array([1, 0, 0, 0, 1, 1, 0, 0, 1]), 'obj2': array([0, 0, 0, 0, 0, 0, 0, 1, 0]), 'obj3': array([1, 1, 1, 0, 0, 1, 1, 1, 0]), 'obj4': array([1, 0, 0, 0, 1, 0, 1, 1, 1])}, 'FSM': None, 'x_func': 11, 'y_func': 57}, 'obj3': {'static': False, 'image_path': None, 'initializer': None, 'init_num_objects': 9, 'update': array([0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0]), 'attributes': {'static': False, 'INIT_VEL': 1, 'MAX_VEL': 4, 'HP': 1, 'MAX_HP': 2}, 'interaction': {'player1': array([1, 1, 1, 0, 0, 1, 1, 1, 0]), 'obj1': array([1, 1, 1, 1, 0, 1, 1, 1, 1]), 'obj2': array([1, 0, 1, 1, 0, 1, 1, 0, 0]), 'obj3': array([0, 1, 0, 1, 0, 0, 0, 1, 1]), 'obj4': array([0, 0, 0, 0, 0, 0, 1, 1, 1])}, 'FSM': None, 'x_func': 76, 'y_func': 42}, 'obj4': {'static': False, 'image_path': None, 'initializer': None, 'init_num_objects': 5, 'update': array([1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0]), 'attributes': {'static': False, 'INIT_VEL': 1, 'MAX_VEL': 2, 'HP': 1, 'MAX_HP': 1}, 'interaction': {'player1': array([1, 1, 1, 1, 0, 0, 1, 1, 1]), 'obj1': array([1, 0, 1, 1, 0, 1, 1, 1, 0]), 'obj2': array([0, 1, 0, 1, 0, 1, 1, 1, 0]), 'obj3': array([1, 1, 1, 0, 1, 0, 0, 0, 1]), 'obj4': array([1, 1, 0, 1, 1, 0, 0, 1, 1])}, 'FSM': None, 'x_func': 30, 'y_func': 42}}, 'win': array([0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0]), 'lose': array([0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1])}

g2 = {'player1': {'static': False, 'image_path': None, 'initializer': None, 'init_num_objects': 1, 'update': array([0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0]), 'attributes': {'alive': True, 'MAX_VEL': 4, 'HP': 5, 'MAX_HP': 8, 'initial_key_values': {'up': 0, 'down': 0, 'left': 0, 'right': 0, 'space': 0}}, 'interaction': {'player1': array([0, 1, 0, 0, 1, 1, 1, 0, 1]), 'obj1': array([1, 0, 0, 1, 1, 0, 0, 0, 1]), 'obj2': array([0, 0, 0, 1, 0, 0, 0, 0, 1]), 'obj3': array([0, 0, 1, 0, 0, 1, 1, 1, 0]), 'obj4': array([0, 1, 1, 0, 1, 1, 1, 0, 1])}, 'FSM': None, 'x_func': 56, 'y_func': 77, 'actions': array([0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0,
       0, 0, 0, 0, 0, 0, 0, 0])}, 'game_objects': {'obj1': {'static': False, 'image_path': None, 'initializer': None, 'init_num_objects': 7, 'update': array([1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1]), 'attributes': {'static': False, 'INIT_VEL': 3, 'MAX_VEL': 4, 'HP': 3, 'MAX_HP': 6}, 'interaction': {'player1': array([0, 1, 1, 1, 0, 0, 1, 0, 1]), 'obj1': array([1, 1, 0, 0, 0, 1, 1, 1, 0]), 'obj2': array([0, 1, 1, 1, 1, 1, 0, 1, 0]), 'obj3': array([0, 0, 1, 0, 1, 0, 0, 1, 0]), 'obj4': array([1, 1, 1, 0, 1, 0, 1, 1, 0])}, 'FSM': None, 'x_func': 14, 'y_func': 20}, 'obj2': {'static': False, 'image_path': None, 'initializer': None, 'init_num_objects': 2, 'update': array([0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1]), 'attributes': {'static': False, 'INIT_VEL': 3, 'MAX_VEL': 3, 'HP': 1, 'MAX_HP': 1}, 'interaction': {'player1': array([0, 0, 1, 0, 0, 1, 1, 0, 1]), 'obj1': array([0, 0, 0, 1, 1, 1, 0, 0, 1]), 'obj2': array([0, 0, 1, 0, 0, 1, 1, 1, 1]), 'obj3': array([0, 1, 1, 0, 1, 0, 1, 0, 0]), 'obj4': array([1, 0, 0, 1, 1, 0, 1, 1, 1])}, 'FSM': None, 'x_func': 55, 'y_func': 42}, 'obj3': {'static': False, 'image_path': None, 'initializer': None, 'init_num_objects': 5, 'update': array([1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0]), 'attributes': {'static': False, 'INIT_VEL': 4, 'MAX_VEL': 4, 'HP': 2, 'MAX_HP': 4}, 'interaction': {'player1': array([0, 0, 0, 1, 1, 0, 1, 1, 1]), 'obj1': array([0, 1, 1, 0, 1, 0, 0, 1, 0]), 'obj2': array([0, 1, 0, 0, 1, 0, 0, 1, 1]), 'obj3': array([0, 0, 0, 1, 0, 1, 0, 0, 0]), 'obj4': array([0, 1, 0, 1, 1, 1, 0, 1, 1])}, 'FSM': None, 'x_func': 57, 'y_func': 35}, 'obj4': {'static': False, 'image_path': None, 'initializer': None, 'init_num_objects': 1, 'update': array([1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1]), 'attributes': {'static': False, 'INIT_VEL': 4, 'MAX_VEL': 4, 'HP': 1, 'MAX_HP': 2}, 'interaction': {'player1': array([1, 0, 0, 1, 0, 1, 1, 1, 0]), 'obj1': array([0, 1, 0, 1, 1, 1, 0, 0, 1]), 'obj2': array([1, 0, 1, 0, 0, 1, 0, 0, 0]), 'obj3': array([0, 0, 1, 0, 0, 0, 1, 1, 1]), 'obj4': array([1, 0, 1, 0, 1, 1, 0, 0, 1])}, 'FSM': None, 'x_func': 92, 'y_func': 70}}, 'win': array([0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0]), 'lose': array([0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0])}
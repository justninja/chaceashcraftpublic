from KnowledgeBase.rule_dict import interactions
from KnowledgeBase.rule_dict import controls
from KnowledgeBase.rule_dict import objectives
from KnowledgeBase.rule_dict import behaviors

from GameApp.DynGameObjectInst import DynGameObjectInst

from panda3d.core import LVector3
from direct.gui.OnscreenText import OnscreenText

from direct.task.Task import Task
from random import random
from math import pi, sin, cos
import sys

#=================================HARD CODED=====================================#

def updatePos(obj, env, dt):
    vel = getVelocity(obj)
    newPos = obj.getPos() + (vel * dt)

    # Check if the object is out of bounds. If so, wrap it
    radius = .5 * obj.getScale().getX()
    if newPos.getX() - radius > env.attributes["SCREEN_X"]:
        newPos.setX(-env.attributes["SCREEN_X"])
    elif newPos.getX() + radius < -env.attributes["SCREEN_X"]:
        newPos.setX(env.attributes["SCREEN_X"])
    if newPos.getZ() - radius > env.attributes["SCREEN_Y"]:
        newPos.setZ(-env.attributes["SCREEN_Y"])
    elif newPos.getZ() + radius < -env.attributes["SCREEN_Y"]:
        newPos.setZ(env.attributes["SCREEN_Y"])
    # elif newPos.getZ() < -10:
    #     newPos.setZ(-10)
    #     setVelocity(obj, vel.project(LVector3.unit_x()))

    obj.setPos(newPos)

def getVelocity(obj):
    return obj.getPythonTag("velocity")

def setVelocity(obj, val):
    obj.setPythonTag("velocity", val)

#================================GENERATED STUFF==================================#

#Generated - Add as many controls[x](env, player, panda_task, dt, objN, x)
#Also possibly generate objN and x as needed. objN may be a spawned object (like a bullet), x may be a rate of acceleration
def check_keys(env, player, panda_task, dt):
    controls[11](env, player, panda_task, dt)
    controls[14](env, player, panda_task, dt)

#Generated - Add the following for each objective (objective x from the list):
# result = objectives[x](env, player, 5)
#     if not result == 0:
#         return result
def check_objectives(env, player):
    result = objectives[2](env, player)
    if not result == 0:
        return result
    result = objectives[22](env, player)
    if not result == 0:
        return result
    return result

#Static
player_panda3d_object = {"tex": "obj1.png", 'scale': 1.0}

#Partially Generated - MAX VEL value can be chosen, but should be included. initial_key_values can always have all possible keys
player_attributes = {
    "alive": True,
    "initial_key_values": {"up": 0, "down": 0, "left": 0, "right": 0, "space": 0, "w": 0, "a": 0, "s": 0, "d": 0},
    "MAX_HP": 5,
    "HP": 3,
    "MAX_VEL": 12
}

#Static - Player always starts motionless
def player_initializer(env, player):
    setVelocity(player.panda3d_object, LVector3.zero())

#Static (may change)
def player_behavior(player, env, panda_task, dt):
    check_keys(env,player,panda_task,dt)
    updatePos(player.panda3d_object, env, dt)

#Generated. For each object, insert:
# elif (other_obj.identifier == "objN"):
#     interactions[x1](player, env)  # , other_obj)
#     interactions[x2](player, env)  # , other_obj)
# List as many interactions as chosen
def player_interact(player, env, other_obj):
    p3do = other_obj.panda3d_object
    if ((player.panda3d_object.getPos() - p3do.getPos()).lengthSquared() <
            (((player.panda3d_object.getScale().getX() + p3do.getScale().getX())
              * .5) ** 2)):
        if (other_obj.identifier == "player"):
            pass
        elif (other_obj.identifier == "obj1"):
            interactions[0](player, env)#, other_obj)
        elif (other_obj.identifier == "obj2"):
            interactions[1](player, env)#, other_obj)
        elif (other_obj.identifier == "obj3"):
            interactions[0](player, env)

#================================================OBJECTS===============================#
# Each function is repeated for each object, just change the number

obj1_panda3d_object = {"tex": "obj1.png", 'scale': 1.0}

#Partially Generated - MAX_VEL can be chosen by the generator
obj1_attributes = {
    "static": False,
    "MAX_HP": 5,
    "HP": 1,
    "INIT_VEL": 3,
    "MAX_VEL": 3
}

#Unclear. Could be generated or follow a pattern. Objects shouldn't start too close to the player
def obj1_initializer(env, obj1GameObj):
    obj1GameObj.container = set()
    # for x in range(5):
    #     obj1GameObj.panda3d_object['tex'] = "obj1.png"
    #     obj1 = DynGameObjectInst(obj1GameObj, env)
    #     obj1GameObj.container.add(obj1)
    #     obj1.panda3d_object.setX(5*x - 1)
    #     obj1.panda3d_object.setZ(-5)
    #
    #     # Heading is a random angle in radians
    #     heading = random() * 2 * pi
    #
    #     # Converts the heading to a vector and multiplies it by speed to
    #     # get a velocity vector
    #     v = LVector3(sin(heading), 0, cos(heading)) * obj1GameObj.attributes['INIT_VEL']
    #     setVelocity(obj1.panda3d_object, v)

#Partially Generated - Object makes adjustments to velocity/heading based on behavior.
def obj1_behavior(obj, env, panda_task, dt):
    behaviors[0](env, obj, panda_task, dt)
    behaviors[13](env, obj, panda_task, dt)
    updatePos(obj.panda3d_object, env, dt)

def obj1_interact(obj1, env, other_obj):
    p3do = other_obj.panda3d_object
    if ((obj1.panda3d_object.getPos() - p3do.getPos()).lengthSquared() <
            (((obj1.panda3d_object.getScale().getX() + p3do.getScale().getX())
              * .5) ** 2)):
        if (other_obj.identifier == "player1"):
            interactions[0](obj1, env)#, other_obj)
        elif (other_obj.identifier == "obj1"):
            interactions[0](obj1, env)#, other_obj)
        elif (other_obj.identifier == "obj2"):
            interactions[1](obj1, env)
            interactions[2](obj1, env)#, other_obj)
        elif (other_obj.identifier == "obj3"):
            interactions[1](obj1, env)
            interactions[2](obj1, env)

obj2_panda3d_object = {"tex": "obj2.png", 'scale': 1.0}

obj2_attributes = {
    "static": False,
    "MAX_HP": 5,
    "HP": 1,
    "INIT_VEL": 2,
    "MAX_VEL": 2
}

def obj2_initializer(env, obj2GameObj):
    obj2GameObj.container = set()
    for x in range(2):
        obj2GameObj.panda3d_object['tex'] = "obj2.png"
        obj2 = DynGameObjectInst(obj2GameObj, env)
        obj2GameObj.container.add(obj2)
        obj2.panda3d_object.setX(5 * x - 15)
        obj2.panda3d_object.setZ(5)

        # Heading is a random angle in radians
        heading = random() * 2 * pi

        # Converts the heading to a vector and multiplies it by speed to
        # get a velocity vector
        v = LVector3(sin(heading), 0, cos(heading)) * obj2GameObj.attributes['INIT_VEL']
        setVelocity(obj2.panda3d_object, v)

def obj2_behavior(obj, env, panda_task, dt):
    behaviors[2](env, obj, panda_task, dt)
    updatePos(obj.panda3d_object, env, dt)

def obj2_interact(obj2, env, other_obj):
    p3do = other_obj.panda3d_object
    if ((obj2.panda3d_object.getPos() - p3do.getPos()).lengthSquared() <
            (((obj2.panda3d_object.getScale().getX() + p3do.getScale().getX())
              * .5) ** 2)):
        if (other_obj.identifier == "player1"):
            interactions[0](obj2, env)#, other_obj)
        elif (other_obj.identifier == "obj1"):
            interactions[1](obj2, env)#, other_obj)
        elif (other_obj.identifier == "obj2"):
            interactions[0](obj2, env)#, other_obj)
        elif (other_obj.identifier == "obj3"):
            interactions[0](obj2, env)

obj3_panda3d_object = {"tex": "obj3.png", 'scale': 1.0}

obj3_attributes = {
    "static": False,
    "MAX_HP": 5,
    "HP": 1,
    "INIT_VEL": 4,
    "MAX_VEL": 4
}

def obj3_initializer(env, obj3GameObj):
    obj3GameObj.container = set()
    for x in range(5):
        obj3GameObj.panda3d_object['tex'] = "obj3.png"
        obj3 = DynGameObjectInst(obj3GameObj, env)
        obj3GameObj.container.add(obj3)
        obj3.panda3d_object.setX(5 * x - 15)
        obj3.panda3d_object.setZ(10)

        # Heading is a random angle in radians
        heading = random() * 2 * pi

        # Converts the heading to a vector and multiplies it by speed to
        # get a velocity vector
        v = LVector3(sin(heading), 0, cos(heading)) * obj3GameObj.attributes['INIT_VEL']
        setVelocity(obj3.panda3d_object, v)

def obj3_behavior(obj, env, panda_task, dt):
    behaviors[2](env, obj, panda_task, dt)
    updatePos(obj.panda3d_object, env, dt)

def obj3_interact(obj3, env, other_obj):
    p3do = other_obj.panda3d_object
    if ((obj3.panda3d_object.getPos() - p3do.getPos()).lengthSquared() <
            (((obj3.panda3d_object.getScale().getX() + p3do.getScale().getX())
              * .5) ** 2)):
        if (other_obj.identifier == "player1"):
            interactions[0](obj3, env)#, other_obj)
        elif (other_obj.identifier == "obj1"):
            interactions[1](obj3, env)#, other_obj)
        elif (other_obj.identifier == "obj2"):
            interactions[0](obj3, env)#, other_obj)
        elif (other_obj.identifier == "obj3"):
            interactions[0](obj3, env)

#===========================================END OBJECTS=====================================#

#Static
player_actions = {"arrow_left": ["left", 1],
                  "arrow_left-up": ["left", 0],
                  "arrow_right": ["right", 1],
                  "arrow_right-up": ["right", 0],
                  "arrow_up": ["up", 1],
                  "arrow_up-up": ["up", 0],
                  "arrow_down": ["down", 1],
                  "arrow_down-up": ["down", 0],
                  "w": ["w", 1],
                  "w-up": ["w", 0],
                  "a": ["a", 1],
                  "a-up": ["a", 0],
                  "s": ["s", 1],
                  "s-up": ["s", 0],
                  "d": ["d", 1],
                  "d-up": ["d", 0],
                  "space": ["space", 1],
                  "space-up": ["space", 0]
                  }

#Static
players = {"player1":
               {"image_path": "player.png",
                "initializer": player_initializer,
                "update": player_behavior,
                "attributes": player_attributes,
                "actions": player_actions,
                "interaction": player_interact,
                "FSM": None,
                }
           }

#Static
game_attributes = {"set_background_color": (0, 0, 0, 1),
                   "bg": {"tex": "stars.jpg", "scale": 146, "depth": 200, "transparency": False},  # "antistars.png"
                   # provides a white background with black spots, if we would rather that
                   "camera.setPosHpr": [0, 0, 25, 0, -90, 0]
                   }

#Generated - One entry in the dictionary for each element
game_objects = {"obj1":
                    {"static": False,
                     "image_path": obj1_panda3d_object,
                     "initializer": obj1_initializer,
                     "update": obj1_behavior,
                     "attributes": obj1_attributes,
                     "interaction": obj1_interact,
                     "FSM": None
                     },
                "obj2":
                    {"static": False,
                     "image_path": obj2_panda3d_object,
                     "initializer": obj2_initializer,
                     "update": obj2_behavior,
                     "attributes": obj2_attributes,
                     "interaction": obj2_interact,
                     "FSM": None
                     },
                "obj3":
                    {"static": False,
                     "image_path": obj3_panda3d_object,
                     "initializer": obj3_initializer,
                     "update": obj3_behavior,
                     "attributes": obj3_attributes,
                     "interaction": obj3_interact,
                     "FSM": None
                     }
                }

#Static
environment_attributes = {"SCREEN_X": 20,
                          "SCREEN_Y": 15,
                          "initial_key_values": {"exit": 0, "restart": 0},
                          "SCORE": 0,
                          "STATE": 0,
                          "score_text_object": None,
                          "win_lose_state": None
                          }

#Static
environment_actions = {"escape": ["exit", 1], "r": ["restart", 1], "r-up": ["restart", 0]}

#Static
def environment_initializer(env):
    env.attributes['score_text_object'] = OnscreenText(text='Score: 0', pos=(-1.18, 0.93), scale=0.07, mayChange=True,
                                                       fg=(1, 1, 0, 1))
    env.attributes['win_lose_state'] = OnscreenText(text='', pos=(0,0,0), scale=0.45,mayChange=True,fg=(1,1,0,1))

#Static
def environment_update(env, panda_task, dt):
    if env.keys['exit']:
        sys.exit()
    if env.attributes['STATE'] == 0:
        env.attributes['STATE'] = check_objectives(env, env.Players['player1'])
        env.attributes['score_text_object'].setText("Score: " + str(env.attributes["SCORE"]))

    if env.attributes['STATE'] != 0 and env.keys['restart']:
        env.attributes['win_lose_state'].setText("")
        env.attributes['score_text_object'].destroy()
        env.restart = True
    elif env.attributes['STATE'] is 1:
        env.attributes['win_lose_state'].setText("YOU WIN")
    elif env.attributes['STATE'] is -1:
        env.attributes['win_lose_state'].setText("GAME OVER\n MAN")

    return Task.cont  # Since every return is Task.cont, the task will
    # continue indefinitely

#Static
def environment_interaction(env, panda_task, dt):
    pass

#Static
environment = {"initializer": environment_initializer,
               "attributes": environment_attributes,
               "players": players,
               "game_objects": game_objects,
               "actions": environment_actions,
               "interaction": environment_interaction,
               "update": environment_update,
               "FSM": None}
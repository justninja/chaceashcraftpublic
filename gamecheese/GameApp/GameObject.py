class GameObject:
    """
    Class representing the general idea of an NPC in our games using Panda3D.
    """

    def __init__(self, identifier, panda3d_object, update_func, attributes, interaction, FSM):
        """
        An abstract Game object.
        :param identifier: probably a string or integer that specifies the type of object this is and how it affects
            the players and other objects in the game.
        :param panda3d_object: (NodePath object if static, list otherwise) The node in the Panda3D scene
            graph if the object is not dynamic (shifts in and out of existence or can have more than one at a time),
            i.e. panda3D's concept of the object in the engine. This can also be a dictionary providing the values of

            loadObject(tex=None, pos=LPoint3(0, 0), depth=SPRITE_POS, scale=1,
                   transparency=True)

            if the object is dynamic. For example, {'tex':'bullet.png', 'scale':0.2}.
        :param update_func: function describing how the object should update at each time step. Can be None if it does
            not do anything.
        :param attributes: (dict) Dictionary of required attributes needed for the object to perform actions and
            interactions
        :param interaction: (func) Function governing the interactions of this object with the players and other
            objects.
        :param FSM: (panda3d FSM object or self created FSM?) Finite state machine governing aspects of player
            behavior.
        """
        self.identifier = identifier
        self.panda3d_object = panda3d_object
        self.update_func = update_func
        self.attributes = attributes.copy()
        self.interaction = interaction
        self.FSM = FSM
        self.container = None
        if not attributes['static']:
            self.container = set()

    def update(self, environment, panda_task, dt):
        if self.update_func is not None:
            if self.attributes['static']:
                return self.update_func(self, environment, panda_task, dt)
            else:
                for go in self.container:
                    go.update(environment, panda_task, dt)

    def interact(self, other_object, environment, panda_task, dt):
        if self.interaction is not None:
            return self.interaction(self, other_object, environment, panda_task, dt)

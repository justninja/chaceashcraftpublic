from direct.showbase.ShowBase import ShowBase
from panda3d.core import GeoMipTerrain
from panda3d.core import LPoint3, LVector3
from panda3d.core import TextNode, TransparencyAttrib
from direct.task.Task import Task
import sys
import os

from importlib import import_module
import glob

from EvaluationCode.GameAgent import GameAgent
from GameApp.Environment import Environment
from GeneratorCode.cce import Generator
from TranslatorCode.EOT import EOT
from GameApp.human_generated_dict import dict as test_dict

SPRITE_POS = 55     # At default field of view and a depth of 55, the screen
# dimensions is 40x30 units
SCREEN_X = 20       # Screen goes from -20 to 20 on X
SCREEN_Y = 15       # Screen goes from -15 to 15 on Y


class GameApp(ShowBase):
    """
    Object containing the functionality to take a generated phenotype and run it in Panda3D for either an AI or a
    player to play.
    """

    def __init__(self, game_title=None):
        self.game = None
        self.Environment = None
        self.Players = {}
        self.GameObjects = {}

        if game_title is not None:
            self.instantiate_game(game_title=game_title)
        else:
            # self.instantiate_game()
            pass

    def generate3d_environment(self, height_field, colormap, world_name):
        """

        :param height_field: (string) File path of the image used for the height field, which is like a contour map.
                            I know gray scale images work, where black is low ground and white is high ground. Other
                            formats are likely possible to allow from more than 255 elevations.
        :param colormap: (string) File path to the image to be overlaid over the height field.
        :param world_name: (string) Name of the environment and filename of the saved .bam file. The '.bam' extension
                            will be added and shouldn't be included in the variable.
        :return: None
        """
        terrain = GeoMipTerrain(world_name)
        terrain.setHeightfield(height_field)
        terrain.setColorMap(colormap)
        terrain.setBruteforce(True)  # forces detailed rendering of image
        root = terrain.getRoot()
        root.reparentTo(self.render)
        root.setSz(60)
        terrain.generate()
        root.writeBamFile(world_name + ".bam")

    # This helps reduce the amount of code used by loading objects, since all of
    # the objects are pretty much the same.
    def loadObject(self, tex=None, pos=LPoint3(0, 0), depth=SPRITE_POS, scale=1,
                   transparency=True):
        """
        Copied code from the asteroids main.py file in the panda3D sample code.
        :param tex:
        :param pos:
        :param depth:
        :param scale:
        :param transparency:
        :return:
        """
        # Every object uses the plane model and is parented to the camera
        # so that it faces the screen.
        plane_file_path = glob.glob('**/plane.egg', recursive=True)[0]  # use glob to find file depending on where
        # GameApp is being run
        obj = self.loader.loadModel(plane_file_path)
        obj.reparentTo(self.camera)

        # Set the initial position and scale.
        obj.setPos(pos.getX(), depth, pos.getY())
        obj.setScale(scale)

        # This tells Panda not to worry about the order that things are drawn in
        # (ie. disable Z-testing).  This prevents an effect known as Z-fighting.
        obj.setBin("unsorted", 0)
        obj.setDepthTest(False)

        if transparency:
            # Enable transparency blending.
            obj.setTransparency(TransparencyAttrib.MAlpha)

        if tex:
            # Load and set the requested texture.
            tex_file_path = glob.glob("**/textures/" + tex, recursive=True)[0]  # use glob to find file depending on
            # where GameApp is being run
            tex = self.loader.loadTexture(tex_file_path)
            obj.setTexture(tex, 1)

        return obj

    def instantiate_game(self, game_title='asteroid_game2'):
        """
        Loads the game model into the GameApp object to prep for running. This allows us to switch games we are running
        without having multiple GameApp objects. Not sure if this is useful or not. I may just put it in the __init__
        function later.
        :param game_title: (string) title of the game to load
        :return: None
        """
        # Code that goes into the knowledge base or generated data and gets everything needed to play the game.
        # Players and GameObjects must be created first and then added to an Environment object, which will be kept
        # track of by this class
        self.game = import_module("GameApp." + game_title)

    def gameLoop(self, task):
        dt = globalClock.getDt()
        res = self.Environment.update(task, dt)
        if res == -1:
            self.userExit()
        else:
            return res
        # note that this function should return something like Task.cont or the game won't loop
        # Currently the code expects Environment.update to call this at some point.

    def run_game_from_file(self, filename):
        """
        Runs the currently loaded game. We may want to add some parameters here later if there end up being different
        ways to run a game.
        :return: None
        """
        # Initialize Panda3D engine
        super().__init__(self)

        self.instantiate_game(filename)

        game_attributes = getattr(self.game, 'game_attributes')

        # Set engine environment stuff
        self.set_background_color(game_attributes["set_background_color"])
        self.bg = self.loadObject(**game_attributes['bg'])
        self.disable_mouse()
        self.camera.setPosHpr(*game_attributes["camera.setPosHpr"])
        # self.accept("escape", sys.exit)
        # base.messenger.toggleVerbose()  # prints reported events to the console

        # Set up Environment class
        environment_vars = getattr(self.game, 'environment')
        self.Environment = Environment(self,
                                       environment_vars['attributes'],
                                       environment_vars['players'],
                                       environment_vars['game_objects'],
                                       environment_vars['actions'],
                                       environment_vars['interaction'],
                                       environment_vars['initializer'],
                                       environment_vars['update'],
                                       environment_vars['FSM'])

        self.gameTask = taskMgr.add(self.gameLoop, 'gameLoop')

        self.run()

    def eval_game(self, game, iterations, game_time_limit, save_info=None, agent_type=None, from_file=False):
        """
        Runs the currently loaded game. We may want to add some parameters here later if there end up being different
        ways to run a game.
        :return: None
        """
        # Initialize Panda3D engine
        super().__init__(self)

        if from_file:
            self.instantiate_game(game_title=game)
            game_attributes = getattr(self.game, 'game_attributes')
            environment_vars = getattr(self.game, 'environment')
        else:
            game_attributes = {"set_background_color": (0, 0, 0, 1),
                               "bg": {"tex": "stars.jpg", "scale": 146, "depth": 200, "transparency": False},
                               # "antistars.png"
                               # provides a white background with black spots, if we would rather that
                               "camera.setPosHpr": [0, 0, 25, 0, -90, 0]
                               }
            environment_translator = EOT(game)
            environment_vars = environment_translator.dictionary()

        # Set engine environment stuff
        self.set_background_color(game_attributes["set_background_color"])
        self.bg = self.loadObject(**game_attributes['bg'])
        self.disable_mouse()
        self.camera.setPosHpr(*game_attributes["camera.setPosHpr"])

        # Set up Environment class

        self.Environment = Environment(self,
                                       environment_vars['attributes'],
                                       environment_vars['players'],
                                       environment_vars['game_objects'],
                                       environment_vars['actions'],
                                       environment_vars['interaction'],
                                       environment_vars['initializer'],
                                       environment_vars['update'],
                                       environment_vars['FSM'])

        self.Environment.game_agent = GameAgent(self.Environment, iterations,
                                                game_time_limit, save_info=save_info, agent_type=agent_type)

        self.gameTask = taskMgr.add(self.gameLoop, 'gameLoop')

        self.run()

    def run_game_from_gen(self, test=False):
        # Initialize Panda3D engine
        super().__init__(self)

        game_attributes = {"set_background_color": (0, 0, 0, 1),
                           "bg": {"tex": "stars.jpg", "scale": 146, "depth": 200, "transparency": False},
                           # "antistars.png"
                           # provides a white background with black spots, if we would rather that
                           "camera.setPosHpr": [0, 0, 25, 0, -90, 0]
                           }

        # Set engine environment stuff
        self.set_background_color(game_attributes["set_background_color"])
        self.bg = self.loadObject(**game_attributes['bg'])
        self.disable_mouse()
        self.camera.setPosHpr(*game_attributes["camera.setPosHpr"])

        # Set up Environment class
        if test:
            generated_environment_vars = test_dict
        else:
            generator = Generator()
            generated_environment_vars = generator.getGame()
        environment_translator = EOT(generated_environment_vars)
        translated_environment_vars = environment_translator.dictionary()
        self.Environment = Environment(self,
                                       translated_environment_vars['attributes'],
                                       translated_environment_vars['players'],
                                       translated_environment_vars['game_objects'],
                                       translated_environment_vars['actions'],
                                       translated_environment_vars['interaction'],
                                       translated_environment_vars['initializer'],
                                       translated_environment_vars['update'],
                                       translated_environment_vars['FSM'])

        self.gameTask = taskMgr.add(self.gameLoop, 'gameLoop')

        self.run()

    def run_game(self, game):
        # Initialize Panda3D engine
        super().__init__(self)

        game_attributes = {"set_background_color": (0, 0, 0, 1),
                           "bg": {"tex": "stars.jpg", "scale": 146, "depth": 200, "transparency": False},
                           # "antistars.png"
                           # provides a white background with black spots, if we would rather that
                           "camera.setPosHpr": [0, 0, 25, 0, -90, 0]
                           }

        # Set engine environment stuff
        self.set_background_color(game_attributes["set_background_color"])
        self.bg = self.loadObject(**game_attributes['bg'])
        self.disable_mouse()
        self.camera.setPosHpr(*game_attributes["camera.setPosHpr"])

        # Set up Environment class
        environment_translator = EOT(game)
        translated_environment_vars = environment_translator.dictionary()
        self.Environment = Environment(self,
                                       translated_environment_vars['attributes'],
                                       translated_environment_vars['players'],
                                       translated_environment_vars['game_objects'],
                                       translated_environment_vars['actions'],
                                       translated_environment_vars['interaction'],
                                       translated_environment_vars['initializer'],
                                       translated_environment_vars['update'],
                                       translated_environment_vars['FSM'])

        self.gameTask = taskMgr.add(self.gameLoop, 'gameLoop')

        self.run()
#
# app = GameApp()
# app.run()

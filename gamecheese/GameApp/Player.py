class Player:
    """
    Class representing the general idea of a player in our games using Panda3D.
    """

    def __init__(self, identifier, panda3d_object, update_func, attributes, actions, interaction, FSM):
        """
        Initialize a player object.
        :param identifier: probably a string or integer that specifies the type of player this is and how it affects
            objects in the game.
        :param panda3d_object: (NodePath object) The node in the Panda3D scene graph, i.e. panda3D's concept of the
            player in the engine.
        :param update_func: function describing how the agent should update at each time step.
        :param attributes: (dict) Dictionary of required attributes needed for the player to perform actions and
            interactions
        :param actions: (dict) Dictionary of key-value pairs where keys are strings corresponding to acceptable keyboard
            or mouse inputs and values are length 2 lists with the first index corresponding to the key name used in the
            update function and the second is the boolean value used to set the key.
            Example: {'arrow_right': ['turn_right", 1], 'arrow_right-up': ['turn_right', 0], 'arrow_right-repeat': ?,
                'mouse1': ['left_click', 1], 'mouse1-up': ['left_click', 0], 'wheel-up': ?]}
        :param interactions: (func) Function performing any player interactions with other players or objects in the
            game.
        :param FSM: (panda3d FSM object or self created FSM?) Finite state machine governing aspects of player
            behavior.
        """
        self.identifier = identifier
        self.panda3d_object = panda3d_object
        self.update_func = update_func
        self.attributes = attributes.copy()
        self.actions = actions.copy()
        self.interaction = interaction
        self.FSM = FSM

    def update(self, Environment, panda_task, dt):
        if self.update_func is not None:
            return self.update_func(self, Environment, panda_task, dt)

    def interact(self, other_object, Environment, panda_task, dt):
        if self.interaction is not None:
            return self.interaction(self, Environment, other_object)
            # (self, other_object, Environment, panda_task, dt)

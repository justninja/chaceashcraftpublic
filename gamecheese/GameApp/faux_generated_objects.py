from panda3d.core import LVector3

from random import randint, choice, random
from math import pi, sin, cos

from GameApp.DynGameObjectInst import DynGameObjectInst
from KnowledgeBase.rule_dict import controls

game_objects = {}

def check_keys(Environment,player,panda_task,dt):
    controls[1](Environment,player,panda_task,dt)
    controls[2](Environment,player,panda_task,dt,'bullet')

class Player:

    def __init__(self, identifier, panda3d_object, update_func, attributes, actions, interaction, FSM):
        self.identifier = identifier
        self.panda3d_object = panda3d_object
        self.update_func = update_func
        self.attributes = attributes
        self.actions = actions
        self.interaction = interaction
        self.FSM = FSM

    def update(self, Environment, panda_task, dt):
        if self.update_func is not None:
            return self.update_func(self, Environment, panda_task, dt)

    def interact(self, other_object, Environment, panda_task, dt):
        if self.interaction is not None:
            return self.interaction(self, other_object, Environment, panda_task, dt)

# # Updates the positions of objects
def updatePos(obj, env, dt):
    vel = getVelocity(obj)
    newPos = obj.getPos() + (vel * dt)

    # Check if the object is out of bounds. If so, wrap it
    radius = .5 * obj.getScale().getX()
    if newPos.getX() - radius > env.attributes["SCREEN_X"]:
        newPos.setX(-env.attributes["SCREEN_X"])
    elif newPos.getX() + radius < -env.attributes["SCREEN_X"]:
        newPos.setX(env.attributes["SCREEN_X"])
    if newPos.getZ() - radius > env.attributes["SCREEN_Y"]:
        newPos.setZ(-env.attributes["SCREEN_Y"])
    elif newPos.getZ() + radius < -env.attributes["SCREEN_Y"]:
        newPos.setZ(env.attributes["SCREEN_Y"])

    obj.setPos(newPos)

#==================================Helper functions=================================#

def setVelocity(obj, val):
    obj.setPythonTag("velocity", val)

def getVelocity(obj):
    return obj.getPythonTag("velocity")

def setExpires(obj, val):
    obj.setPythonTag("expires", val)


def getExpires(obj):
    return obj.getPythonTag("expires")

#===================================Bad functions===================================#

# The handler when an asteroid is hit by a bullet
def asteroidHit(asteroid, env):
    # If the asteroid is small it is simply removed
    if asteroid.panda3d_object.getScale().getX() <= asteroid.parent.attributes['AST_MIN_SCALE']:
        #asteroid.panda3d_object.removeNode()
        # Remove the asteroid from the list of asteroids.
        asteroid.remove_self()
        # asteroid.parent.container.remove(asteroid)
    else:
        # If it is big enough, divide it up into little asteroids.
        # First we update the current asteroid.
        newScale = asteroid.panda3d_object.getScale().getX() * asteroid.parent.attributes['AST_SIZE_SCALE']
        asteroid.panda3d_object.setScale(newScale)  # Rescale it

        # The new direction is chosen as perpendicular to the old direction
        # This is determined using the cross product, which returns a
        # vector perpendicular to the two input vectors.  By crossing
        # velocity with a vector that goes into the screen, we get a vector
        # that is orthagonal to the original velocity in the screen plane.
        vel = getVelocity(asteroid.panda3d_object)
        speed = vel.length() * asteroid.parent.attributes['AST_VEL_SCALE']
        vel.normalize()
        vel = LVector3(0, 1, 0).cross(vel)
        vel *= speed
        setVelocity(asteroid.panda3d_object, vel)

        # Now we create a new asteroid identical to the current one
        newAst_p3d0 = env.gameApp.loadObject(env.GameObjects[asteroid.identifier]
                                                     .panda3d_object['tex'], scale=newScale)
        newAst = DynGameObjectInst(asteroid.parent, env, panda3d_object=newAst_p3d0)
        setVelocity(newAst.panda3d_object, vel * -1)
        newAst.panda3d_object.setPos(asteroid.panda3d_object.getPos())
        newAst.panda3d_object.setTexture(asteroid.panda3d_object.getTexture(), 1)
        asteroid.parent.container.add(newAst)

def spawn_asteroids(env, asteroidGameObject):  # spawnAsteroids() function in asteroids.py sample file
    # Control variable for if the ship is alive
    env.Players['player1'].attributes['alive'] = True
    asteroidGameObject.container = set()  # set container, here just for emphasis
    for i in range(10):
        # This loads an asteroid. The texture chosen is random
        # from "asteroid1.png" to "asteroid3.png".
        asteroidGameObject.panda3d_object['tex'] = "asteroid%d.png" % (randint(1, 3))  # hack to get randomness...
        asteroid = DynGameObjectInst(asteroidGameObject, env)
        asteroidGameObject.container.add(asteroid)

        # This is kind of a hack, but it keeps the asteroids from spawning
        # near the player.  It creates the list (-20, -19 ... -5, 5, 6, 7,
        # ... 20) and chooses a value from it. Since the player starts at 0
        # and this list doesn't contain anything from -4 to 4, it won't be
        # close to the player.
        asteroid.panda3d_object.setX(choice(tuple(range(-env.attributes['SCREEN_X'], -5))
                                            + tuple(range(5, env.attributes['SCREEN_X']))))
        # Same thing for Y
        asteroid.panda3d_object.setZ(choice(tuple(range(-env.attributes['SCREEN_Y'], -5))
                                            + tuple(range(5, env.attributes['SCREEN_Y']))))

        # Heading is a random angle in radians
        heading = random() * 2 * pi

        # Converts the heading to a vector and multiplies it by speed to
        # get a velocity vector
        v = LVector3(sin(heading), 0, cos(heading)) * asteroidGameObject.attributes['AST_INIT_VEL']
        setVelocity(asteroid.panda3d_object, v)

#==================================Object functions=================================#

asteroid_attributes = {"static": False,
                       "AST_INIT_SCALE": 3,
                       "AST_INIT_VEL": 1,
                       "AST_VEL_SCALE": 2.2,
                       "AST_SIZE_SCALE": 0.6,
                       "AST_MIN_SCALE": 1.1
                       }

bullet_attributes = {"static": False,  # this attribute should be in all objects, just in case it is useful
                     "DEG_TO_RAD": pi / 180,
                     "SPEED": 10,
                     "LIFETIME": 2,
                     "BULLET_REPEAT": 0.2,
                     "nextBullet": 0.0
                     }

bullet_panda3d_object = {"tex": "bullet.png", 'scale': 0.2}

asteroid_panda3d_object = {'tex': "asteroid%d.png" % (randint(1, 3)), 'scale': asteroid_attributes['AST_INIT_SCALE']}

def bullet_initializer(env, bulletGameOb):
    pass

def asteroid_initializer(env, asteroidGameOb):
    spawn_asteroids(env, asteroidGameOb)

def bullet_update(bullet, env, panda_task, dt):
    updatePos(bullet.panda3d_object, env, dt)  # Update the bullet
    # Bullets have an expiration time (see definition of fire)
    # If a bullet has not expired, add it to the new bullet list so
    # that it will continue to exist.
    if getExpires(bullet.panda3d_object) <= panda_task.time:
        # bullet.panda3d_object.removeNode()
        # env.stack_interaction(bullet.panda3d_object.removeNode, [])
        # env.stack_interaction(bullet.parent.container.remove, [bullet])
        # bullet.parent.container.remove(bullet)
        env.stack_interaction(bullet.remove_self, [])

def asteroid_update(asteroid, env, panda_task, dt):
    updatePos(asteroid.panda3d_object, env, dt)

def bullet_interaction(bullet, other_object, env, panda_task, dt):
    if other_object.identifier == "asteroid":
        asteroid_p3do = other_object.panda3d_object
        # Panda's collision detection is more complicated than we need
        # here.  This is the basic sphere collision check. If the
        # distance between the object centers is less than sum of the
        # radii of the two objects, then we have a collision. We use
        # lengthSquared() since it is faster than length().
        if ((bullet.panda3d_object.getPos() - asteroid_p3do.getPos()).lengthSquared() <
                (((bullet.panda3d_object.getScale().getX() + asteroid_p3do.getScale().getX())
                  * .5) ** 2)):
            # Schedule the bullet for removal
            setExpires(bullet.panda3d_object, 0)

def asteroid_interaction(asteroid, other_object, env, panda_task, dt):
    if other_object.identifier == 'bullet':
        bullet = other_object.panda3d_object
        if ((bullet.getPos() - asteroid.panda3d_object.getPos()).lengthSquared() <
                    (((bullet.getScale().getX() + asteroid.panda3d_object.getScale().getX())
                      * .5) ** 2)):
            env.stack_interaction(asteroidHit, [asteroid, env])

#==================================Object dictionary================================#

game_objects = {"bullet":
                    {"static": False,
                     "image_path": bullet_panda3d_object,
                     "initializer": bullet_initializer,
                     "update": bullet_update,
                     "attributes": bullet_attributes,
                     "interaction": bullet_interaction,
                     "FSM": None
                     },
                "asteroid":
                    {"static": False,
                     "image_path": asteroid_panda3d_object,
                     "initializer": asteroid_initializer,
                     "update": asteroid_update,
                     "attributes": asteroid_attributes,
                     "interaction": asteroid_interaction,
                     "FSM": None
                     }
                }
"""
File containing the relevant data for the asteroids game. First set up the functions and variables used
to define the parameters passed into the GameApp object, then define the parameters.
"""

from panda3d.core import LVector3
from direct.interval.MetaInterval import Sequence
from direct.interval.FunctionInterval import Wait, Func
from direct.task.Task import Task

from GameApp.GameObject import GameObject
from GameApp.DynGameObjectInst import DynGameObjectInst
from GameApp.Player import Player
from GameApp.Environment import Environment

from math import pi, sin, cos
from random import randint, choice, random
import sys

# environment functions and Variables

environment_attributes = {"SCREEN_X": 20,
                          "SCREEN_Y": 15,
                          "initial_key_values": {"exit": 0},
                          "SCORE": 0
                          }

environment_actions = {"escape": ["exit", 1]}


def environment_initializer(env):
    pass


def environment_update(env, panda_task, dt):
    if env.keys['exit']:
        sys.exit()

    if len(env.GameObjects['asteroid'].container) == 0:
        spawn_asteroids(env, env.GameObjects['asteroid'])

    return Task.cont  # Since every return is Task.cont, the task will
    # continue indefinitely


def environment_interaction(env, panda_task, dt):
    pass


# Players Functions and Variables

player_attributes = {"alive": True,
                     "TURN_RATE": 360,
                     "DEG_TO_RAD": pi/180,
                     "ACCELERATION": 10,
                     "MAX_VEL_SQ": 6**2,
                     "MAX_VEL": 6,
                     "initial_key_values": {"turnLeft": 0, "turnRight": 0, "accel": 0, "fire": 0}
                     }

player_actions = {"arrow_left": ["turnLeft", 1],
                  "arrow_left-up": ["turnLeft", 0],
                  "arrow_right": ["turnRight", 1],
                  "arrow_right-up": ["turnRight", 0],
                  "arrow_up": ["accel", 1],
                  "arrow_up-up": ["accel", 0],
                  "space": ["fire", 1]
                  }


def setVelocity(obj, val):
    obj.setPythonTag("velocity", val)


def getVelocity(obj):
    return obj.getPythonTag("velocity")


def setExpires(obj, val):
    obj.setPythonTag("expires", val)


def getExpires(obj):
    return obj.getPythonTag("expires")


# Updates the positions of objects
def updatePos(obj, env, dt):
    vel = getVelocity(obj)
    newPos = obj.getPos() + (vel * dt)

    # Check if the object is out of bounds. If so, wrap it
    radius = .5 * obj.getScale().getX()
    if newPos.getX() - radius > env.attributes["SCREEN_X"]:
        newPos.setX(-env.attributes["SCREEN_X"])
    elif newPos.getX() + radius < -env.attributes["SCREEN_X"]:
        newPos.setX(env.attributes["SCREEN_X"])
    if newPos.getZ() - radius > env.attributes["SCREEN_Y"]:
        newPos.setZ(-env.attributes["SCREEN_Y"])
    elif newPos.getZ() + radius < -env.attributes["SCREEN_Y"]:
        newPos.setZ(env.attributes["SCREEN_Y"])

    obj.setPos(newPos)


def setAttribute(obj, key, new_value):
    obj[key] = new_value


def restart_game(ship, ast, env):
        env.Players['player1'].attributes['alive'] = False  # Ship is no longer alive
        # Remove every object in asteroids and bullets from the scene
        for i in (list(ast.parent.container)
                  + list(env.GameObjects['bullet'].container)):
            i.panda3d_object.removeNode()
        env.GameObjects["bullet"].container.clear()  # Clear the bullet list
        ast.parent.container.clear()
        ship.hide()  # Hide the ship
        # Re-initialize the initial key values, if needed -- not needed in this case
        # for set_key, in_val in Environment.Players['player1'].attributes['initial_key_values'].items():
        #     Environment.keys[set_key] = in_val
        # Reset the velocity
        setVelocity(ship, LVector3(0, 0, 0))
        Sequence(Wait(2),  # Wait 2 seconds
                 Func(ship.setR, 0),  # Reset heading
                 Func(ship.setX, 0),  # Reset position X
                 # Reset position Y (Z for Panda)
                 Func(ship.setZ, 0),
                 Func(ship.show),  # Show the ship
                 Func(spawn_asteroids, *[env, ast.parent])).start()  # Remake asteroids
        return Task.cont


def player_update(player, env, panda_task, dt):
    if not player.attributes['alive']:
        return Task.cont

    keys = env.keys
    heading = player.panda3d_object.getR()  # Heading is the roll value for this model
    # Change heading if left or right is being pressed
    if keys["turnRight"]:
        heading += dt * player.attributes["TURN_RATE"]
        player.panda3d_object.setR(heading % 360)
    elif keys["turnLeft"]:
        heading -= dt * player.attributes["TURN_RATE"]
        player.panda3d_object.setR(heading % 360)

    # Thrust causes acceleration in the direction the ship is currently
    # facing
    if keys["accel"]:
        heading_rad = player.attributes["DEG_TO_RAD"] * heading

        # This builds a new velocity vector and adds it to the current one
        # relative to the camera, the screen in Panda is the XZ plane.
        # Therefore all of our Y values in our velocities are 0 to signify
        # no change in that direction.
        newVel = \
            LVector3(sin(heading_rad), 0, cos(heading_rad)) * player.attributes["ACCELERATION"] * dt
        newVel += getVelocity(player.panda3d_object)

        # Clamps the new velocity to the maximum speed. lengthSquared() is
        # used again since it is faster than length()
        if newVel.lengthSquared() > player.attributes["MAX_VEL_SQ"]:
            newVel.normalize()
            newVel *= player.attributes["MAX_VEL"]
        setVelocity(player.panda3d_object, newVel)

    # check to see if the ship can fire
    bulletGameOb = env.GameObjects['bullet']
    if env.keys["fire"] and panda_task.time > bulletGameOb.attributes['nextBullet']:
        fire(bulletGameOb, env, panda_task.time)  # If so, call the fire function
        # And disable firing for a bit
        bulletGameOb.attributes['nextBullet'] = panda_task.time + bulletGameOb.attributes['BULLET_REPEAT']
    # Remove the fire flag until the next spacebar press
    env.keys["fire"] = 0

    # Finally, update the position as with any other object
    updatePos(player.panda3d_object, env, dt)


def player_initializer(env, player):
    setVelocity(player.panda3d_object, LVector3.zero())


def player_interaction(playerOb, other_object, env, panda_task, dt):
    if other_object.identifier == "asteroid":
        ast = other_object
        ship = playerOb.panda3d_object

        shipSize = ship.getScale().getX()
        if ((ship.getPos() - ast.panda3d_object.getPos()).lengthSquared() <
                (((shipSize + ast.panda3d_object.getScale().getX()) * .5) ** 2)):
            # If there is a hit, clear the screen and schedule a restart
            env.stack_interaction(restart_game, [ship, ast, env])

# Objects functions and variables

bullet_attributes = {"static": False,  # this attribute should be in all objects, just in case it is useful
                     "DEG_TO_RAD": pi / 180,
                     "BULLET_SPEED": 10,
                     "BULLET_LIFE": 2,
                     "BULLET_REPEAT": 0.2,
                     "nextBullet": 0.0
                     }

bullet_panda3d_object = {"tex": "bullet.png", 'scale': 0.2}


def bullet_initializer(env, bulletGameOb):
    pass


# Creates a bullet and adds it to the bullet list
def fire(bulletGameOb, env, time):
    ship = env.Players['player1'].panda3d_object
    direction = bulletGameOb.attributes["DEG_TO_RAD"] * ship.getR()
    pos = ship.getPos()
    bullet = DynGameObjectInst(bulletGameOb, env)
    bullet.panda3d_object.setPos(pos)
    # Velocity is in relation to the ship
    vel = (getVelocity(ship) +
           (LVector3(sin(direction), 0, cos(direction)) *
            bulletGameOb.attributes['BULLET_SPEED']))
    setVelocity(bullet.panda3d_object, vel)
    # Set the bullet expiration time to be a certain amount past the
    # current time
    setExpires(bullet.panda3d_object, time + bulletGameOb.attributes['BULLET_LIFE'])

    # Finally, add the new bullet to the list
    env.stack_interaction(bulletGameOb.container.add, [bullet])


def bullet_update(bullet, env, panda_task, dt):
    updatePos(bullet.panda3d_object, env, dt)  # Update the bullet
    # Bullets have an expiration time (see definition of fire)
    # If a bullet has not expired, add it to the new bullet list so
    # that it will continue to exist.
    if getExpires(bullet.panda3d_object) <= panda_task.time:
        # bullet.panda3d_object.removeNode()
        env.stack_interaction(bullet.panda3d_object.removeNode, [])
        env.stack_interaction(bullet.parent.container.remove, [bullet])
        # bullet.parent.container.remove(bullet)


def bullet_interaction(bullet, other_object, env, panda_task, dt):
    if other_object.identifier == "asteroid":
        asteroid_p3do = other_object.panda3d_object
        # Panda's collision detection is more complicated than we need
        # here.  This is the basic sphere collision check. If the
        # distance between the object centers is less than sum of the
        # radii of the two objects, then we have a collision. We use
        # lengthSquared() since it is faster than length().
        if ((bullet.panda3d_object.getPos() - asteroid_p3do.getPos()).lengthSquared() <
                (((bullet.panda3d_object.getScale().getX() + asteroid_p3do.getScale().getX())
                  * .5) ** 2)):
            # Schedule the bullet for removal
            setExpires(bullet.panda3d_object, 0)


asteroid_attributes = {"static": False,
                       "AST_INIT_SCALE": 3,
                       "AST_INIT_VEL": 1,
                       "AST_VEL_SCALE": 2.2,
                       "AST_SIZE_SCALE": 0.6,
                       "AST_MIN_SCALE": 1.1
                       }

asteroid_panda3d_object = {'tex': "asteroid%d.png" % (randint(1, 3)), 'scale': asteroid_attributes['AST_INIT_SCALE']}


# The handler when an asteroid is hit by a bullet
def asteroidHit(asteroid, env):
    # If the asteroid is small it is simply removed
    if asteroid.panda3d_object.getScale().getX() <= asteroid.parent.attributes['AST_MIN_SCALE']:
        asteroid.panda3d_object.removeNode()
        # Remove the asteroid from the list of asteroids.
        asteroid.parent.container.remove(asteroid)
    else:
        # If it is big enough, divide it up into little asteroids.
        # First we update the current asteroid.
        newScale = asteroid.panda3d_object.getScale().getX() * asteroid.parent.attributes['AST_SIZE_SCALE']
        asteroid.panda3d_object.setScale(newScale)  # Rescale it

        # The new direction is chosen as perpendicular to the old direction
        # This is determined using the cross product, which returns a
        # vector perpendicular to the two input vectors.  By crossing
        # velocity with a vector that goes into the screen, we get a vector
        # that is orthagonal to the original velocity in the screen plane.
        vel = getVelocity(asteroid.panda3d_object)
        speed = vel.length() * asteroid.parent.attributes['AST_VEL_SCALE']
        vel.normalize()
        vel = LVector3(0, 1, 0).cross(vel)
        vel *= speed
        setVelocity(asteroid.panda3d_object, vel)

        # Now we create a new asteroid identical to the current one
        newAst_p3d0 = env.gameApp.loadObject(env.GameObjects[asteroid.identifier]
                                                     .panda3d_object['tex'], scale=newScale)
        newAst = DynGameObjectInst(asteroid.parent, env, panda3d_object=newAst_p3d0)
        setVelocity(newAst.panda3d_object, vel * -1)
        newAst.panda3d_object.setPos(asteroid.panda3d_object.getPos())
        newAst.panda3d_object.setTexture(asteroid.panda3d_object.getTexture(), 1)
        asteroid.parent.container.add(newAst)


def spawn_asteroids(env, asteroidGameObject):  # spawnAsteroids() function in asteroids.py sample file
    # Control variable for if the ship is alive
    env.Players['player1'].attributes['alive'] = True
    asteroidGameObject.container = set()  # set container, here just for emphasis
    for i in range(10):
        # This loads an asteroid. The texture chosen is random
        # from "asteroid1.png" to "asteroid3.png".
        asteroidGameObject.panda3d_object['tex'] = "asteroid%d.png" % (randint(1, 3))  # hack to get randomness...
        asteroid = DynGameObjectInst(asteroidGameObject, env)
        asteroidGameObject.container.add(asteroid)

        # This is kind of a hack, but it keeps the asteroids from spawning
        # near the player.  It creates the list (-20, -19 ... -5, 5, 6, 7,
        # ... 20) and chooses a value from it. Since the player starts at 0
        # and this list doesn't contain anything from -4 to 4, it won't be
        # close to the player.
        asteroid.panda3d_object.setX(choice(tuple(range(-env.attributes['SCREEN_X'], -5))
                                            + tuple(range(5, env.attributes['SCREEN_X']))))
        # Same thing for Y
        asteroid.panda3d_object.setZ(choice(tuple(range(-env.attributes['SCREEN_Y'], -5))
                                            + tuple(range(5, env.attributes['SCREEN_Y']))))

        # Heading is a random angle in radians
        heading = random() * 2 * pi

        # Converts the heading to a vector and multiplies it by speed to
        # get a velocity vector
        v = LVector3(sin(heading), 0, cos(heading)) * asteroidGameObject.attributes['AST_INIT_VEL']
        setVelocity(asteroid.panda3d_object, v)


def asteroid_update(asteroid, env, panda_task, dt):
    updatePos(asteroid.panda3d_object, env, dt)


def asteroid_interaction(asteroid, other_object, env, panda_task, dt):
    if other_object.identifier == 'bullet':
        bullet = other_object.panda3d_object
        if ((bullet.getPos() - asteroid.panda3d_object.getPos()).lengthSquared() <
                    (((bullet.getScale().getX() + asteroid.panda3d_object.getScale().getX())
                      * .5) ** 2)):
            env.stack_interaction(asteroidHit, [asteroid, env])


def asteroid_initializer(env, asteroidGameOb):
    spawn_asteroids(env, asteroidGameOb)

# Parameters used by the GameApp object vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

game_attributes = {"set_background_color": (0, 0, 0, 1),
                   "bg": {"tex": "stars.jpg", "scale": 146, "depth": 200, "transparency": False},  # "antistars.png"
                   # provides a white background with black spots, if we would rather that
                   "camera.setPosHpr": [0, 0, 25, 0, -90, 0]
                   }

players = {"player1":
               {"image_path": "ship.png",
                    "initializer": player_initializer,
                "update": player_update,
                "attributes": player_attributes,
                "actions": player_actions,
                "interaction": player_interaction,
                "FSM": None,
                }
           }

game_objects = {"bullet":
                    {"static": False,
                     "image_path": bullet_panda3d_object,
                     "initializer": bullet_initializer,
                     "update": bullet_update,
                     "attributes": bullet_attributes,
                     "interaction": bullet_interaction,
                     "FSM": None
                     },
                "asteroid":
                    {"static": False,
                     "image_path": asteroid_panda3d_object,
                     "initializer": asteroid_initializer,
                     "update": asteroid_update,
                     "attributes": asteroid_attributes,
                     "interaction": asteroid_interaction,
                     "FSM": None
                     }
                }

environment = {"initializer": environment_initializer,
               "attributes": environment_attributes,
               "players": players,
               "game_objects": game_objects,
               "actions": environment_actions,
               "interaction": environment_interaction,
               "update": environment_update,
               "FSM": None}

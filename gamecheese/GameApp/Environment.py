from GameApp.Player import Player
from GameApp.GameObject import GameObject
from GameApp.DynGameObjectInst import DynGameObjectInst

from direct.task.Task import Task


class Environment:
    """
    Class representing the environment and how it interacts with panda3d, the players and the objects.
    """

    def __init__(self, gameApp, attributes, players, game_objects, actions, interactions, initializer, update_func,
                 FSM=None):
        """
        Initialize an environment object.
        :param gameApp: Reference to the GameApp object with all the panda3d stuff
        :param attributes: (dict) Dictionary of required attributes needed for the player to perform actions and
            interactions
        :param players: (dict) Dictionary of player identifiers and Player data.
        :param game_objects: (dict) Dictionary of object identifiers and GameObject data.
        :param actions: (dict) dictionary where keys are strings and values are functions or a list of a function and
            a list of inputs to the function.
            Example: {'k-repeat': [type_k_continually, ['string',6],'k-up':print_new_line, 'escape':sys.exit}
        :param interactions: (func) Function governing interactions with the players, objects, and the environment.
            In general, this set should declare special types of interactions with the players, the objects, and the
            environment, stuff not included in the objects' and players' update, action, and interaction functions.
            Likely to be empty.
        :param initializer: (func) Function that initializes any special environment behaviors.
        :param update_func: (func) Function describing how the object should update at each time step. Can be None if
            it does not do anything
        :param FSM: (panda3d FSM object or self created FSM?) Finite state machine governing aspects of environments
            behavior.
        """
        self.gameApp = gameApp
        self.attributes = None  # set these to copies in the load_and_set function
        self.actions = None
        self.interactions = interactions
        self.initializer = initializer
        self.update_func = update_func
        self.FSM = FSM

        self.init_attributes = attributes
        self.init_actions = actions
        self.init_players = players
        self.init_game_objects = game_objects

        self.Players = {}
        self.GameObjects = {}
        self.keys = {}
        self.interaction_stack = []

        # restart helpers
        self.waiting_for_restart = False
        self.restart = False

        self.game_agent = None  # GameAgent(self)  # Set this to turn on the agent

        self.load_and_set()

    def update(self, panda_task, dt):
        """
        Update the simulation one time step, where dt=length of the time step.
        :param panda_task: (panda3d.core.PythonTask) so far, used to get the time since the game started.
        :param dt: (float?) time difference between last time step and this one.
        :return: (Task conclusion object like Task.cont) This is required for the gameloop to go to next cycle.
        """
        if self.game_agent is not None:
            res = self.game_agent.play_game(panda_task, dt)
            if res == -1:
                return -1

        if self.attributes['STATE'] != 0:
            if self.restart and self.waiting_for_restart:
                self.load_and_set()
                self.waiting_for_restart = False
                self.restart = False
                self.attributes['STATE'] = 0
                return Task.cont
            elif self.waiting_for_restart:
                return self.update_func(self, panda_task, dt)
            else:
                self.clear_all_objects()
                self.waiting_for_restart = True
                return Task.cont

        for player in self.Players.values():
            out = player.update(self, panda_task, dt)
            if out is not None:
                return out
        for game_object in self.GameObjects.values():
            out = game_object.update(self, panda_task, dt)
            if out is not None:
                return out
        self.stack_interactions(panda_task, dt)
        out = self.perform_interactions()
        if out is not None:
            return out
        return self.update_func(self, panda_task, dt)

    # may need to rewrite this function to just pass in the set of things to interact with
    # in case interactions required knowing about more than one object or player
    def stack_interactions(self, panda_task, dt):
        """
        Go through the agents, objects and environment and perform all the appropriate interactions.
        :return: None or a Task object value
        """
        all_objects = list(self.Players.values())
        for game_object in self.GameObjects.values():
            all_objects += list(game_object.container)

        for i in range(len(all_objects)):
            for j in range(i+1, len(all_objects)):
                all_objects[i].interact(all_objects[j], self, panda_task, dt)
                all_objects[j].interact(all_objects[i], self, panda_task, dt)

        # old interactions code
        # for player in self.Players.values():
        #     out = player.interact(self, panda_task, dt)
        #     if out is not None:
        #         return out
        # for game_object in self.GameObjects.values():
        #     out = game_object.interact(self, panda_task, dt)
        #     if out is not None:
        #         return out

    def setKey(self, key, val):
        self.keys[key] = val

    def new_player(self, playerId, player_dict):
        p = Player(playerId,
                   self.gameApp.loadObject(player_dict['image_path']),
                   player_dict["update"],
                   player_dict['attributes'],
                   player_dict['actions'],
                   player_dict['interaction'],
                   player_dict['FSM'])
        player_dict['initializer'](self, p)
        return p

    def new_static_object(self, objectId, object_dict):
        go = GameObject(objectId,
                        self.gameApp.loadObject(object_dict["image_path"]),
                        object_dict['update'],
                        object_dict['attributes'],
                        object_dict['interaction'],
                        object_dict['FSM'])
        object_dict['initializer'](self, go)
        return go

    def new_dynamic_object(self, objectId, object_dict):
        go = GameObject(objectId,
                        object_dict["image_path"],
                        object_dict['update'],
                        object_dict['attributes'],
                        object_dict['interaction'],
                        object_dict['FSM'])
        object_dict['initializer'](self, go)
        return go

    def stack_interaction(self, func, args_list):
        self.interaction_stack.append([func, args_list])

    def perform_interactions(self):
        """
        Go through stacked interactions, then empty stack.
        :return:
        """
        for i in range(len(self.interaction_stack)):
            interaction = self.interaction_stack[i]
            func = interaction[0]
            args = interaction[1]
            not_removed = True
            for arg in args:
                if isinstance(arg, DynGameObjectInst) and arg.removed:
                    # arg.removed = False  # in asteroids, some objects are kept and this is needed
                    # I suppose for our games, we will always assume that all objects are destroyed and
                    # recreated, because this isn't working otherwise
                    not_removed = False
            if not_removed:
                out = func(*args)
                if out is not None:
                    self.interaction_stack = []
                    return out

        self.interaction_stack = []

    def load_and_set(self):
        self.attributes = self.init_attributes.copy()
        self.actions = self.init_actions.copy()

        # initialize any special environment stuff, if needed
        self.initializer(self)

        # set keys used by the environment
        for action in self.actions.keys():
            self.gameApp.accept(action, self.setKey, self.actions[action])
        # initialize environment key values
        for key, val in self.attributes['initial_key_values'].items():
            self.keys[key] = val

        # initialize players and any keys they will use
        for player in self.init_players.keys():
            self.Players[player] = self.new_player(player, self.init_players[player])
            action_dict = self.Players[player].actions
            for set_key, in_val in self.Players[player].attributes['initial_key_values'].items():
                self.keys[set_key] = in_val
            for action in action_dict.keys():
                self.gameApp.accept(action, self.setKey, action_dict[action])

        # initialize game objects: Static objects persist the whole game, dynamic ones come and go and many may exist
        # at once
        for game_object in self.init_game_objects.keys():
            if self.init_game_objects[game_object]['static']:
                self.GameObjects[game_object] = self.new_static_object(game_object, self.init_game_objects[game_object])
            else:
                self.GameObjects[game_object] = self.new_dynamic_object(game_object,
                                                                        self.init_game_objects[game_object])

    def clear_all_objects(self):
        for player in self.Players.values():
            player.panda3d_object.removeNode()
        self.Players.clear()
        for go in self.GameObjects.values():
            if not go.attributes['static']:
                for obj in list(go.container):
                    obj.remove_self()
            else:
                go.panda3d_object.removeNode()
        self.GameObjects.clear()


